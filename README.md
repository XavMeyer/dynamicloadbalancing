# README #

Simple dynamic load balancing manager using c++ / mpi / boost.

##  How to compile ? ##

Requires :

* MPI
* Boost headers
* Serialization library from boost

## How to use ? ##

The main file (loadBalancing.cpp) contains an example of utilisation.


For a custom application, you should define your own job that should inherit from the virtual class Jobs/Job.
There are some pure virtual function to implement for input / output such as :

* fromString / toString
* serialize / unserialize


To use the job manager your need first to instantiate the class

 * LoadBalancing::LBCommMgr

which is the "communicatior / mpi" class.


Then you must instantiate the Job Manager

 * LoadBalancing::JobManager<LoadBalancing::MyJob>

that require you to specify your Job class.


Then your can ask for jobs using the Job Manager function

 * T* JobManager::askNextJob()

that return a pointer to a job.


When your job is done you should signal it using Job Manager function

 * void signalJobDone();