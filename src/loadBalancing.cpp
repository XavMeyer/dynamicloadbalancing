/*
 ============================================================================
 Name        : LoadBalancing.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello MPI World in C 
 ============================================================================
 */
#include <stdio.h>
#include <string.h>
#include <algorithm>
#include <set>
#include "mpi.h"
#include <boost/random/normal_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>
//#include "LoadBalancingMixte.h"

#include "CPP/LoadBalancing/Jobs/ExampleJob.h"
#include "CPP/LoadBalancing/LBCommMgr.h"
#include "CPP/LoadBalancing/JobManager.hpp"

/*void testCodeC(int argc, char* argv[]) {
	int  myRank, nProc;
	ptrJob_t ptrJob;
	float coeff; int cntJob = 0;
	double t1, t2;

	//
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
	MPI_Comm_size(MPI_COMM_WORLD, &nProc);

	srand (myRank);
	coeff = 50.+100.*((float)rand()/RAND_MAX);
	//printf(stdout, "[%d] Coeff : %f \n", myRank, coeff);

	if(myRank == 0) {
		coeff *= 2.;
	} else {
		coeff *= 1.;
	}

	t1 = MPI_Wtime();
	fprintf(stdout, "[%d] INIT LBRID\n", myRank);
	initLBRID(myRank, nProc);

	while(askNextJob(&ptrJob)){

		if(cntJob % 50 == 0){
			fprintf(stdout, "[%d] Job at checkpoint : %d \n", myRank, cntJob);
		}

		//printf("[%d] Job : ", myRank);
		printJob();
		//t1 = MPI_Wtime();
		usleep(5.e2*ptrJob->time*coeff);
		//t2 = MPI_Wtime();
		//printf("[%d] Job processed in %f ms.\n", myRank, (t2-t1)*1.e3);

		//t1 = MPI_Wtime();
		signalJobDone();
		//t2 = MPI_Wtime();
		//printf("[%d] Load balancing done in %f ms.\n", myRank, (t2-t1)*1.e3);
		cntJob++;
		fflush(stdout);
	}

	//waitNeighbors();

	fprintf(stdout, "[%d] DEINIT LBRID\n", myRank);
	deInitLBRID();
	t2 = MPI_Wtime();
	printf("[%d] Work done in %f s.\n", myRank, (t2-t1));

	// shut down MPI
	MPI_Finalize();

}*/

enum typeLoad_t {CORRECT_LOAD=0, ESTIMATED_LOAD=1, UNKOWN_LOAD=2};

void generateJobFiles(size_t nProc, size_t avgJobPerProc, typeLoad_t typeLoad, double VARIANCE = 30, size_t SEED=1) {
	double muTime = 100;
	double sigmaTime = 15;
	double sigmaMeanTime = VARIANCE;

	boost::random::mt19937 rng(SEED);

	boost::random::normal_distribution<double> distMeanTime(muTime, sigmaMeanTime);
	boost::random::normal_distribution<double> distError(0, 2.5);

	std::vector<double> vecJobs;

	for(size_t iP=0; iP<nProc; ++iP) {

		std::stringstream ssFileCP;
		ssFileCP << "chkpt/cp_" << std::setw(5) << std::setfill('0') << iP;
		std::ofstream oFileCP(ssFileCP.str().c_str());
		oFileCP.close();

		boost::random::normal_distribution<double> distTime(distMeanTime(rng), sigmaTime);
		std::stringstream ssFile;
		ssFile << "jobs/job_" << std::setw(5) << std::setfill('0') << iP;
		std::ofstream oFile(ssFile.str().c_str());

		std::stringstream ssFileOriginal;
		ssFileOriginal << "oJobs/job_" << std::setw(5) << std::setfill('0') << iP;
		std::ofstream oFileOrig(ssFileOriginal.str().c_str());

		for(size_t iJ=0; iJ<avgJobPerProc; ++iJ) {
			LoadBalancing::ExampleJob job;
			double time = std::max(distTime(rng), 0.5);
			job.setTime(time);
			vecJobs.push_back(time);

			double load;
			if(typeLoad == CORRECT_LOAD) {
				load = job.getTime();
			} else if(typeLoad == ESTIMATED_LOAD) {
				load = distTime.mean();
			} else {
				load = 5.0;
			}
			job.setLoad(load);

			oFile << job.toString() << std::endl;
			oFileOrig << job.toString() << std::endl;
		}
		oFile.close();
		oFileOrig.close();
	}

	// Quick and dirty static scheduling
	std::sort(vecJobs.begin(), vecJobs.end(), std::greater<double>());
	std::vector<double> procLoad(nProc, 0.);
	for(size_t i=0; i<vecJobs.size(); ++i) {
		std::vector<double>::iterator it = std::min_element(procLoad.begin(), procLoad.end());
		*it += vecJobs[i];
	}
	double minLoad = *std::min_element(procLoad.begin(), procLoad.end());
	double maxLoad = *std::max_element(procLoad.begin(), procLoad.end());

	std::cout << "Min/Max time static : " << minLoad << " /  " << maxLoad << std::endl;


}

void readResults(std::set<size_t> &originalJobsId, std::set<size_t> &processedJobsId,
				 std::vector<double> &origTime, std::vector<double> &lbTime) {

	double minJob=std::numeric_limits<double>::max(), maxJob = 0.;

	for(size_t iP=0; iP<origTime.size(); ++iP) {
		std::string str;

		// Checkpoint files
		std::stringstream ssFileCP;
		ssFileCP << "chkpt/cp_" << std::setw(5) << std::setfill('0') << iP;
		std::ifstream iFileCP(ssFileCP.str().c_str());

		while (std::getline(iFileCP, str)) {
			if(!str.empty()) {
				LoadBalancing::ExampleJob job;
				job.fromString(str);
				lbTime[iP] += job.getTime();
				assert(processedJobsId.find(job.getId()) == processedJobsId.end());
				processedJobsId.insert(job.getId());
				minJob = std::min(minJob, job.getTime());
				maxJob = std::max(maxJob, job.getTime());
			}
		}

		std::stringstream ssFileOriginal;
		ssFileOriginal << "oJobs/job_" << std::setw(5) << std::setfill('0') << iP;
		std::ifstream iFileOrig(ssFileOriginal.str().c_str());

		while (std::getline(iFileOrig, str)) {
			if(!str.empty()) {
				LoadBalancing::ExampleJob job;
				job.fromString(str);
				origTime[iP] += job.getTime();
				assert(originalJobsId.find(job.getId()) == originalJobsId.end());
				originalJobsId.insert(job.getId());
			}
		}
	}

	std::cout << "Min job time = " << minJob << " && Max job time = " << maxJob << std::endl;
}


double getSum(const std::vector<double> &vec) {
	double sum = 0.;
	for(size_t i=0; i<vec.size(); ++i) {
		sum += vec[i];
	}
	return sum;
}

double getMean(const std::vector<double> &vec) {
	double sum = getSum(vec);
	return sum / (double)vec.size();
}

double getVariance(const std::vector<double> &vec) {
    double mean = getMean(vec);
    double temp = 0;
    for(size_t i=0; i<vec.size(); ++i)
        temp += (mean-vec[i])*(mean-vec[i]);
    return temp/(double)vec.size();
}



void checkResult(size_t nProc) {

	std::set<size_t> originalJobsId, processedJobsId;
	std::vector<double> origTime(nProc, 0.), lbTime(nProc, 0.);
	readResults(originalJobsId, processedJobsId, origTime, lbTime);

	if(originalJobsId.size() != processedJobsId.size()) {
		std::vector<size_t> diff;
		std::set_symmetric_difference(
				originalJobsId.begin(), originalJobsId.end(),
				processedJobsId.begin(), processedJobsId.end(),
		        std::back_inserter(diff));

		std::cout << "Jobs that are missing : ";
		for(size_t i=0; i<diff.size(); ++i) {
			std::cout << diff[i] << ", ";
		}
		std::cout << std::endl;
		abort();
	}

	std::cout << "Min/Max time without LB : " << *std::min_element(origTime.begin(), origTime.end());
	std::cout << " / " << *std::max_element(origTime.begin(), origTime.end());
	std::cout << " with mean = " << getMean(origTime) << " with std = " << sqrt(getVariance(origTime)) << std::endl;
	std::cout << "Min/Max time with LB : " << *std::min_element(lbTime.begin(), lbTime.end());
	std::cout << " / " << *std::max_element(lbTime.begin(), lbTime.end());
	std::cout << " with mean = " << getMean(lbTime) << " with std = " << sqrt(getVariance(lbTime)) << std::endl;

	/*for(size_t iP=0; iP<nProc; ++iP) {
		std::cout << iP << " :: " << origTime[iP] << " :: " << lbTime[iP] << std::endl;
	}*/
}


void testCodeCPP(int argc, char* argv[]) {
	LoadBalancing::LBCommMgr lbMgr(&argc, &argv, LoadBalancing::NeighborhoodFactory::NBRHOOD_3D);

	srand (lbMgr.getMyRank());

	if(lbMgr.getMyRank() == 0) {
		generateJobFiles(lbMgr.getNProc(), 50, ESTIMATED_LOAD);
	}
	MPI_Barrier(MPI_COMM_WORLD);

	{
		LoadBalancing::JobManager<LoadBalancing::ExampleJob> jobManager("jobs/job_", "chkpt/cp_", lbMgr);

		double coeff = 1.e3;

		double t1, t2;
		LoadBalancing::ExampleJob *ptr;
		t1 = MPI_Wtime();
		while((ptr = jobManager.askNextJob()) != NULL){

			/*if(cntJob % 50 == 0){
				fprintf(stdout, "[%d] Job at checkpoint : %d \n", myRank, cntJob);
			}*/

			//printf("[%d] Job : ", myRank);
			//printJob();
			//double sProcess = MPI_Wtime();
			//std::cout << "[" << lbMgr.getMyRank() << "] My load = " << std::setw(4) << std::setfill(' ') <<  jobManager.getCurrentLoad() << " -- Processing job " << ptr->getId() << " with load = " << ptr->getLoad() << std::endl;
			usleep(coeff*ptr->getTime());
			//double eProcess = MPI_Wtime();
			//printf("[%d] Job processed in %f ms.\n", lbMgr.getMyRank(), (eProcess-sProcess)*1.e3);

			//t1 = MPI_Wtime();
			jobManager.signalJobDone();
			//t2 = MPI_Wtime();
			//printf("[%d] Load balancing done in %f ms.\n", myRank, (t2-t1)*1.e3);
			//cntJob++;
			fflush(stdout);
		}
		t2 = MPI_Wtime();
		printf("[%d] Work done in %f s.\n", lbMgr.getMyRank(), (t2-t1));
	}

	MPI_Barrier(MPI_COMM_WORLD);
	if(lbMgr.getMyRank() == 0) {
		checkResult(lbMgr.getNProc());
	}

}

void benchCodeCPP(int argc, char* argv[]) {

	size_t LOAD_TYPE = atoi(argv[1]);
	size_t NB_TYPE = atoi(argv[2]);
	size_t N_JOB = atoi(argv[3]);
	double VARIANCE = atof(argv[4]);
	double COEFF = atof(argv[5]);
	size_t SEED = atoi(argv[6]);



	LoadBalancing::LBCommMgr lbMgr(&argc, &argv, static_cast<LoadBalancing::NeighborhoodFactory::neighborhoodType_t>(NB_TYPE));

	srand (lbMgr.getMyRank());

	if(lbMgr.getMyRank() == 0) {
		generateJobFiles(lbMgr.getNProc(), N_JOB, static_cast<typeLoad_t>(LOAD_TYPE), VARIANCE, SEED);
	}
	MPI_Barrier(MPI_COMM_WORLD);

	{
		LoadBalancing::JobManager<LoadBalancing::ExampleJob> jobManager("jobs/job_", "chkpt/cp_", lbMgr);

		double coeff = COEFF;

		double t1, t2;
		LoadBalancing::ExampleJob *ptr;
		t1 = MPI_Wtime();
		while((ptr = jobManager.askNextJob()) != NULL){

			/*if(cntJob % 50 == 0){
				fprintf(stdout, "[%d] Job at checkpoint : %d \n", myRank, cntJob);
			}*/

			//printf("[%d] Job : ", myRank);
			//printJob();
			//double sProcess = MPI_Wtime();
			//std::cout << "[" << lbMgr.getMyRank() << "] My load = " << std::setw(4) << std::setfill(' ') <<  jobManager.getCurrentLoad() << " -- Processing job " << ptr->getId() << " with load = " << ptr->getLoad() << std::endl;
			usleep(coeff*ptr->getTime());
			//double eProcess = MPI_Wtime();
			//printf("[%d] Job processed in %f ms.\n", lbMgr.getMyRank(), (eProcess-sProcess)*1.e3);

			//t1 = MPI_Wtime();
			jobManager.signalJobDone();
			//t2 = MPI_Wtime();
			//printf("[%d] Load balancing done in %f ms.\n", myRank, (t2-t1)*1.e3);
			//cntJob++;
			fflush(stdout);
		}
		t2 = MPI_Wtime();
		//printf("[%d] Work done in %f s.\n", lbMgr.getMyRank(), (t2-t1));
	}

	MPI_Barrier(MPI_COMM_WORLD);
	if(lbMgr.getMyRank() == 0) {
		checkResult(lbMgr.getNProc());
	}

}

int main(int argc, char* argv[]){
	//testCodeC(argc, argv);
	//testCodeCPP(argc, argv);
	benchCodeCPP(argc, argv);
	
	return 0;
}
