/*
 * LoadBalancingMIXTE.c
 *
 *  Created on: 28 juin 2013
 *      Author: meyerx
 */
#include "LoadBalancingMixte.h"

#define OFFSETOF(TYPE, ELEMENT) ((size_t)&(((TYPE *)0)->ELEMENT))


/*************************************************************************
 ******************           Local variables       *********************
 ************************************************************************/
int nProc, myRank, myStatus;
long checkpoint;
double lowThreshold, diffThreshold, comLoad;
char jobFName[LONG_STR_LEN], checkPtFName[LONG_STR_LEN];
neighbor_t nbrInfo[NB_NEIGHBORS];
sendBuff_t sendBuff[NB_NEIGHBORS];

// MPI
int mpiBufferSize;
char *mpiBuffer;
MPI_Datatype jobType;


/*************************************************************************
 *******************    Local functions headers    ***********************
 ************************************************************************/
// init
void initNbr1D();
void initNbr2D();
#if TYPE_NEIGHBORHOOD == NEIGHBORHOOD_3D
void initNbr3D();
#endif
#if TYPE_NEIGHBORHOOD == NEIGHBORHOOD_BGQ
int defineHisRank(int *myCoords, int dim, int offset);
void initNbrBGQ();
#endif
void initNbr();
void initMpiBuffer();
void initMpiType();
void initThreshold();
void initCheckpoint();

// Check for messages and process them with the
// appropriate receive function
void waitMessages(int *isVectorUpdated);
void checkPendingMessages(int *isVectorUpdated);

// Send the current load of the thread to its neighbor.
// Check, receive and update load status (nbrInfo).
void sendLoadStatus(double load);
void receiveLoadStatus(MPI_Status *status);

// Send a job request to nbrId for "size" load
// Receive the job request and update the nbrInfo
int countPendingJobRequest();
void sendJobRequest(int nbrId, double size);
void receiveJobRequest(MPI_Status *status);

// Send and receive jobs.
// When sending, the jobs must be inserted into sendBuff
// When receiving an array of job is created
// cleanSentJobs look for pending send completion and clean.
void sendJobs(int nbrId);
void receiveJobs(MPI_Status *status, int *isVectorUpdated);
void cleanSentJobs(int immediate);

// End signal to nbr
void sendMyStatus();
void receiveStatus(MPI_Status *status);

// Balancing function
void answerRequests(int *isVectorUpdated);
void balanceLoadRID();
void balanceLoadSID(int *isVectorUpdated);

// utils
int procId2NbrId(int src);
int areAllNbrSID();
void saveCheckPoint(int isVectorUpdated);

/*************************************************************************
 ******************          Global functions         *********************
 ************************************************************************/

// Global functions
void initLBRID(int _myRank, int _nProc){
	myRank = _myRank;
	nProc = _nProc;
	myStatus = STATUS_RID;
	// init Neighborhood
	initNbr();

	// Init mpi things
	initMpiBuffer();
	initMpiType();

	// Init FName
	sprintf(jobFName, "%s%s%s%05u", DATA_PATH, JOB_DIR, PREFIX_JOB, myRank);
	sprintf(checkPtFName, "%s%s%s%05u", DATA_PATH, CHKPT_DIR, PREFIX_CHKPT, myRank);

	// Init JobVector
	initJobVector(jobFName);

	// Init Threshold
	initThreshold();

	// Init checkpoint
	initCheckpoint();

	// Send load to neighbors
	comLoad = getTotalLoad();
	sendLoadStatus(comLoad);
}


// Change here if we want a fancy job selection
int askNextJob(ptrJob_t *jobPtr){
	int isVectorUpdated;

	// Get current job
	getJob(jobPtr);

	// If the vector is empty...
	if(*jobPtr == NULL){
		// ... and my status is RID
		if(myStatus == STATUS_RID ){
			// I wait for all pending request
			while(countPendingJobRequest() > 0){
				// Blocking
				answerRequests(&isVectorUpdated);
				isVectorUpdated = 0;
				waitMessages(&isVectorUpdated);
				// Has the message updated the vector ?
				if(isVectorUpdated){
					// we save the new state of job vector
					writeToFile(jobFName);
					getJob(jobPtr);
					return *jobPtr != NULL;
				} // else we wait for more message
			}
			// no more pending job request and still no jobs, we switch to SID
			myStatus = STATUS_SID;
			sendMyStatus();
		}
		if(myStatus == STATUS_SID){
			// I wait for all my neighbors to be in SID mode
			while(!areAllNbrSID()){
				// Blocking
				answerRequests(&isVectorUpdated);
				isVectorUpdated = 0;
				waitMessages(&isVectorUpdated);
				// Has the message updated the vector ?
				if(isVectorUpdated){
					// we save the new state of job vector
					writeToFile(jobFName);
					getJob(jobPtr);
					return *jobPtr != NULL;
				} // else we wait for more message
			}
			// All my neighbors are waiting for push, I'm done
			myStatus = STATUS_FINISHED;
			//sendMyStatus();
		}
	}

	return *jobPtr != NULL;
}

void signalJobDone(){
	int isVectorUpdated = 0;
	// Signal job done so we skip to next
	++checkpoint;
	jobDone();

	// Check messages
	checkPendingMessages(&isVectorUpdated);

	// Answer requests (RID mode)
	answerRequests(&isVectorUpdated);

	// Clean buffer for sent jobs (1 ==> not blocking)
	cleanSentJobs(1);

	// Then we look if we have to ask for jobs...
	if(myStatus == STATUS_RID){
		balanceLoadRID();
	}

	// ..or to push some (depend on neighbor status)
	balanceLoadSID(&isVectorUpdated);

	// Send status if load variation is greater than 10%
	if((getTotalLoad() > comLoad/UPDATE_FACTOR) || (getTotalLoad() < comLoad*UPDATE_FACTOR)){
		comLoad = getTotalLoad();
		sendLoadStatus(comLoad);
	}

	// Save checkpoint
	saveCheckPoint(isVectorUpdated);
}

void deInitLBRID(){

	// Deinit JobVector
	writeToFile(jobFName);
	deInitJobVector();

	// Cleaning pending com (not immediate)
	cleanSentJobs(0);

	// Detach mpi buffer and free
	MPI_Buffer_detach(mpiBuffer, &mpiBufferSize);
	free(mpiBuffer);
}


/*************************************************************************
 *******************       Local functions body    ***********************
 ************************************************************************/

void initNbr1D(){
	nbrInfo[0].id = (myRank+1) % nProc;
	nbrInfo[1].id = (nProc+myRank-1) % nProc;
}

void initNbr2D(){
	int myX, myY, size, mySx, mySy, nFullLastL;
	div_t divRes;
	// Get the size of a line / column
	size = ceil(sqrt((float)nProc));
	// Get x,y coordinate in function of myRank
	divRes = div(myRank, size);
	myX = divRes.quot;
	myY = divRes.rem;

	mySx = mySy = size;
	nFullLastL = size - ((size*size)-nProc);
	assert(nFullLastL > 2);
	// Number of elements on the y axis (may be diff for last line)
	if(myX == (size-1)){ // last line
		mySy = nFullLastL; // nFull last line
	}
	// Number of elements on the x axis (may be diff for some cols)
	if(myY >= nFullLastL){ // not full cols
		mySx = size - 1; // size -1
	}
	// Left neighbor
	nbrInfo[0].id = ((mySy+myY-1)%mySy)+myX*size;
	// Right neighbor
	nbrInfo[1].id = ((myY+1)%mySy)+myX*size;
	// Up neighbor
	nbrInfo[2].id = myY+((mySx+myX-1)%mySx)*size;
	// Down neighbor
	nbrInfo[3].id = myY+((myX+1)%mySx)*size;
	//printf("[%d] My neighbors are : x=[%d] y=[%d] sX=[%d] sY[%d] l=[%d] r=[%d] u=[%d] d=[%d]\n", myRank, myX, myY, mySx, mySy, nbrInfo[0].id, nbrInfo[1].id, nbrInfo[2].id, nbrInfo[3].id);
	//sleep(5);
}

#if TYPE_NEIGHBORHOOD == NEIGHBORHOOD_3D_16P
void initNbr3D(){
	int myX, myY, myZ, dimSizeZ;
	div_t divResZ, divResXY, divResNProc;
	// We assume that we have at least 3*PLANE_SIZE processors this leads to
	assert(nProc >= (3*PLANE_SIZE));
	divResNProc = div(nProc, PLANE_SIZE);
	assert(divResNProc.rem == 0);

	// First we divide by the size of a X-Y plane
	// PLANE_SIZE is given by X_DIM_SIZE * Y_DIM_SIZE
	divResZ = div(myRank, PLANE_SIZE);
	// We get the z position
	myZ = divResZ.quot;

	// Then we divide the remainder by the Y_DIM_SIZE
	divResXY = div(divResZ.rem, Y_DIM_SIZE);
	myY = divResXY.quot;
	myX = divResXY.rem;

	dimSizeZ = divResNProc.quot;
	// TODO MORE GENERAL CASE
	// Now we have to look if the last plane is full
	/*if(divResNProc.rem == N_PROC_NODE){
	} else if(divResNProc.rem == 2*N_PROC_NODE){
	} else if(divResNProc.rem == 3*N_PROC_NODE){
	}*/

	// x,y,z-1
	nbrInfo[0].id = myX+(myY*Y_DIM_SIZE)+((myZ+dimSizeZ-1) % dimSizeZ)*PLANE_SIZE;
	// x,y,z+1
	nbrInfo[1].id = myX+(myY*Y_DIM_SIZE)+((myZ+1) % dimSizeZ)*PLANE_SIZE;
	// x,y-1,z
	nbrInfo[2].id = myX+(((myY+Y_DIM_SIZE-1)%Y_DIM_SIZE)*Y_DIM_SIZE)+(myZ*PLANE_SIZE);
	// x,y+1,z
	nbrInfo[3].id = myX+(((myY+1)%Y_DIM_SIZE)*Y_DIM_SIZE)+(myZ*PLANE_SIZE);
	// x-1,y,z
	nbrInfo[4].id = ((myX+X_DIM_SIZE-1)%X_DIM_SIZE)+(myY*Y_DIM_SIZE)+(myZ*PLANE_SIZE);
	// x+1,y,z
	nbrInfo[5].id = ((myX+1)%X_DIM_SIZE)+(myY*Y_DIM_SIZE)+(myZ*PLANE_SIZE);

	/*printf("[%d] My neighbors are : x=[%d] y=[%d] z=[%d] n1=[%d] n2=[%d] n3=[%d] n4=[%d] n5=[%d] n6=[%d]\n",
			myRank, myX, myY, myZ, nbrInfo[0].id, nbrInfo[1].id, nbrInfo[2].id, nbrInfo[3].id,
			nbrInfo[4].id, nbrInfo[5].id);*/
	//sleep(5);
}
#endif

#if TYPE_NEIGHBORHOOD == NEIGHBORHOOD_BGQ
int defineHisRank(int *myCoords, int dim, int offset){
	int myVal, hisRank, myX, myY;
	div_t divRes;

	if(dim == X_DIM || dim == Y_DIM){
		divRes = div(myCoords[5], Y_DIM_SIZE);
		myX = divRes.quot;
		myY = divRes.rem;
	}

	switch (dim) {
		case X_DIM:
			myVal = myCoords[5];
			myCoords[5] = myY + ((myX+offset+X_DIM_SIZE)%X_DIM_SIZE)*Y_DIM_SIZE;
			// MPIX_Torus2rank(myCoords, &hisRank);
			myCoords[5] = myVal;
			break;
		case Y_DIM:
			myVal = myCoords[5];
			myCoords[5] = (myY+offset+Y_DIM_SIZE)%Y_DIM_SIZE + myX*Y_DIM_SIZE;
			// MPIX_Torus2rank(myCoords, &hisRank);
			myCoords[5] = myVal;
			break;
		case E_DIM:
			myVal = myCoords[4];
			myCoords[4] = (myCoords[4]+offset+E_DIM_SIZE)%E_DIM_SIZE;
			// MPIX_Torus2rank(myCoords, &hisRank);
			myCoords[4] = myVal;
			break;
		case D_DIM:
			myVal = myCoords[3];
			myCoords[3] = (myCoords[3]+offset+D_DIM_SIZE)%D_DIM_SIZE;
			// MPIX_Torus2rank(myCoords, &hisRank);
			myCoords[3] = myVal;
			break;
		case C_DIM:
			myVal = myCoords[2];
			myCoords[2] = (myCoords[2]+offset+C_DIM_SIZE)%C_DIM_SIZE;
			// MPIX_Torus2rank(myCoords, &hisRank);
			myCoords[2] = myVal;
			break;
		case B_DIM:
			myVal = myCoords[1];
			myCoords[1] = (myCoords[1]+offset+B_DIM_SIZE)%B_DIM_SIZE;
			// MPIX_Torus2rank(myCoords, &hisRank);
			myCoords[1] = myVal;
			break;
		case A_DIM:
			myVal = myCoords[0];
			myCoords[0] = (myCoords[0]+offset+A_DIM_SIZE)%A_DIM_SIZE;
			// MPIX_Torus2rank(myCoords, &hisRank);
			myCoords[0] = myVal;
			break;
		default:
			hisRank = -1;
			break;
	}
	return hisRank;
}

void initNbrBGQ(){
	int numDim=0;
	int *coords=NULL;

	// Ask for the torus dimensions
	//MPIX_Torus_ndims(&numDim);

	// Get the coordinates from the rank
	// Coordinates c : (<c[0]=A{4-8}, c[1]=B{4}, c[2]=C{4}, c[3]=D{4}, c[4]=E{2}, c[5]=T{1...64}>)
	coords = (int *)malloc((numDim+1)*sizeof(int));
	//MPIX_Rank2torus(myRank, coords);

#if N_NODE >= 128
	// Define neighbors
	// X(T) and Y(T) +1/-1
	nbrInfo[0].id = defineHisRank(coords, X_DIM, +1);
	nbrInfo[1].id = defineHisRank(coords, X_DIM, -1);
	nbrInfo[2].id = defineHisRank(coords, Y_DIM, +1);
	nbrInfo[3].id = defineHisRank(coords, Y_DIM, -1);
	// E is max dim 2
	nbrInfo[4].id = defineHisRank(coords, E_DIM, +1);
	// D is max dim 4
	nbrInfo[5].id = defineHisRank(coords, D_DIM, +1);
	nbrInfo[6].id = defineHisRank(coords, D_DIM, -1);
	// C is max dim 4
	nbrInfo[7].id = defineHisRank(coords, C_DIM, +1);
	nbrInfo[8].id = defineHisRank(coords, C_DIM, -1);
	// B is max dim 4
	nbrInfo[9].id = defineHisRank(coords, B_DIM, +1);
	nbrInfo[10].id = defineHisRank(coords, B_DIM, -1);
#endif
#if N_NODE >= 512
	// A is max dim 4
	nbrInfo[11].id = defineHisRank(coords, A_DIM, +1);
	nbrInfo[12].id = defineHisRank(coords, A_DIM, -1)
#endif
#if N_NODE >= 1024
	// TODO See dimension a (max size 8) as a 4x2 array with 3 neighbors per process
#endif

	/* TODO use
	 *	MPIX_Init_hw(MPIX_Hardware_t *hw)
	 *	This function takes an MPIX_Hardware_t structure, as defined in
	 *	mpix.h, and completes the fields. The hardware structure provides:
	 *	The physical rank irrespective of mapping
	 *	The size of the block irrespective of mapping
	 *	The number of processes per node
	 *	The core-thread ID of this process
	 *	The frequency of the processor clock
	 *	The size of the memory on the compute node
	 *	The number of torus dimensions
	 *	The size of each torus dimension
	 *	The torus coordinates of this process
	 *	A wrap-around link attribute for each torus dimension
	 *	int MPIX_Torus_ndims(int *numdimensions)
	 *	This function returns the dimensionality of the torus (typically five on
	 *	the Blue Gene/Q system).
	 *	int MPIX_Rank2torus(int rank, int *coords)
	 *	This function returns the torus physi
	 *	cal coordinates in the coords array
	 *	for the MPI_COMM_WORLD rank passed in. The coords array needs
	 *	to be predeclared and preallocated. It has the size numdimensions+1
	 *	(typically six on the Blue Gene/Q system).
	 *	int MPIX_Torus2rank(int *coords, int *rank)
	 *	This function returns the MPI_CO
	 *	MM_WORLD rank for the passed in
	 *	torus coordinates. The coords array needs to be of size
	 *	numdimensions+1 (typically six on the Blue Gene/Q system)
	 */

	free(coords);
}
#endif

void initNbr(){
	int iNbr;

#if TYPE_NEIGHBORHOOD == NEIGHBORHOOD_1D
	initNbr1D();
#elif TYPE_NEIGHBORHOOD == NEIGHBORHOOD_2D
	initNbr2D();
#elif TYPE_NEIGHBORHOOD == NEIGHBORHOOD_3D_16P
	initNbr3D();
#elif TYPE_NEIGHBORHOOD == NEIGHBORHOOD_BGQ
	initNbrBGQ();
#endif

	for(iNbr=0; iNbr < NB_NEIGHBORS; ++iNbr){
		//printf("[%d] My neighbor is : %d \n", myRank, nbrInfo[iNbr].id);
		nbrInfo[iNbr].load = -1;
		nbrInfo[iNbr].hisReqStatus = HIS_JOB_REQ_NONE;
		nbrInfo[iNbr].myReqStatus = MY_JOB_REQ_NONE;
		nbrInfo[iNbr].status = STATUS_RID;
		sendBuff[iNbr].size = -1;
		sendBuff[iNbr].jobs = NULL;
	}
	//sleep(5);

}

void initMpiBuffer(){
	int sizeDouble;
	// Reserve the buffer size for the outgoing messages
	MPI_Pack_size( 1, MPI_DOUBLE, MPI_COMM_WORLD, &sizeDouble);
	// 4 * nNeigbors * message type (load status + job request + finished) * overhead * buffSize
	mpiBufferSize = 4 * NB_NEIGHBORS * 3 * MPI_BSEND_OVERHEAD * sizeDouble;
	mpiBuffer = (char *)malloc( mpiBufferSize );
	MPI_Buffer_attach( mpiBuffer, mpiBufferSize );
}

void initMpiType(){
	job_t tmp;
	MPI_Datatype type[7] = { MPI_CHAR, MPI_CHAR, MPI_INT, MPI_INT, MPI_INT, MPI_INT, MPI_FLOAT };
	int blockLen[7] = { STR_LEN, STR_LEN, 1, 1, 1, 1, 1};
	MPI_Aint disp[7];

	// init disp
    disp[0] = OFFSETOF(job_t, alignement); //(void*)tmp.alignement - (void*)&tmp;
    disp[1] = OFFSETOF(job_t, tree); //(void*)tmp.tree - (void*)&tmp;
    disp[2] = OFFSETOF(job_t, pos1); //(void*)&tmp.pos1 - (void*)&tmp;
    disp[3] = OFFSETOF(job_t, pos2); //(void*)&tmp.pos2 - (void*)&tmp;
    disp[4] = OFFSETOF(job_t, nCSS); //(void*)&tmp.nCSS - (void*)&tmp;
    disp[5] = OFFSETOF(job_t, nLeaf); //(void*)&tmp.nLeaf - (void*)&tmp;
    disp[6] = OFFSETOF(job_t, time); //(void*)&tmp.time - (void*)&tmp;
    MPI_Type_create_struct(7, blockLen, disp, type, &jobType);
    MPI_Type_commit(&jobType);
}

void initThreshold(){
	lowThreshold = RATIO_LOW_THRESH * getTotalLoad();
#if TYPE_RATIO_DIFF == RATIO_DIFF_1PCT
	diffThreshold = 0.1 * getTotalLoad();
#elif TYPE_RATIO_DIFF == RATIO_DIFF_MEAN
	diffThreshold = 2.*getMeanTaskGranularity();
#endif
	fprintf(stdout, "[%d] My thresholds (%f) : low = %f  and diff =  %f.\n", myRank, getTotalLoad(), lowThreshold, diffThreshold);
}

void initCheckpoint(){
	char tmpLine[STR_LEN];
	FILE *checkPtFile;

	// By default it start at 0
	checkpoint = 0;

	// Check if checkpoint file exists
	checkPtFile = fopen(checkPtFName, "r");
	// It the file exist we read the current job to process
	if(checkPtFile!=NULL){
		fgets(tmpLine, sizeof(tmpLine), checkPtFile);
		sscanf (tmpLine, "%li", &checkpoint);
		fclose(checkPtFile);
	}
	//printf("[%d] Checkpoint at line : %ld\n", myRank, curJob);

	if(checkpoint > 0){
		skipToJob(checkpoint);
		fprintf(stdout, "[%d] Starting at checkpoint : %ld with %f remaining work.\n", myRank, checkpoint, getTotalLoad());
	}

}

int procId2NbrId(int src){
	int iNbr;
	// Not optimized but work
	for(iNbr=0; iNbr<NB_NEIGHBORS; ++iNbr){
		if(nbrInfo[iNbr].id == src){
			return iNbr;
		}
	}
	return -1;
}

void waitMessages(int *isVectorUpdated){
	MPI_Status status;

	MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
	switch (status.MPI_TAG) {
		case TAG_COM_LOAD:
			//fprintf(stdout, "[%d] Processing a LOAD msg from blocking wait.\n", myRank);
			receiveLoadStatus(&status);
			break;
		case TAG_REQUEST:
			//fprintf(stdout, "[%d] Processing a REQ msg from blocking wait.\n", myRank);
			receiveJobRequest(&status);
			break;
		case TAG_SEND_JOBS:
			//fprintf(stdout, "[%d] Processing a JOB msg from blocking wait.\n", myRank);
			receiveJobs(&status, isVectorUpdated);
			break;
		case TAG_STATUS:
			//fprintf(stdout, "[%d] Processing a STATUS msg from blocking wait.\n", myRank);
			receiveStatus(&status);
			break;
		default:
			//fprintf(stdout, "[%d] Processing a ERR msg from blocking wait.\n", myRank);
			fprintf(stderr, "Erroneous MPI message received.");
			raise(SIGINT);
			break;
	}
}

void checkPendingMessages(int *isVectorUpdated){
	int flag;
	MPI_Status status;

	MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &status);
	while(flag){
		switch (status.MPI_TAG) {
			case TAG_COM_LOAD:
				receiveLoadStatus(&status);
				break;
			case TAG_REQUEST:
				receiveJobRequest(&status);
				break;
			case TAG_SEND_JOBS:
				receiveJobs(&status, isVectorUpdated);
				break;
			case TAG_STATUS:
				receiveStatus(&status);
				break;
			default:
				fprintf(stderr, "Erroneous MPI message received.");
				raise(SIGINT);
				break;
		}
		MPI_Iprobe(MPI_ANY_SOURCE, TAG_COM_LOAD, MPI_COMM_WORLD, &flag, &status);
	}
}


void sendLoadStatus(double load){
	int iNbr;
	for(iNbr = 0; iNbr < NB_NEIGHBORS; ++iNbr){
		// Buffered  send (non blocking)
		if(nbrInfo[iNbr].status == STATUS_RID){
			MPI_Bsend(&load, 1, MPI_DOUBLE, nbrInfo[iNbr].id, TAG_COM_LOAD, MPI_COMM_WORLD);
		}
	}
}

void receiveLoadStatus(MPI_Status *status){
	int src;
	double load;
	// Check if there is pending messages
   	src = status->MPI_SOURCE;
   	// Consume message
   	MPI_Recv(&load, 1, MPI_DOUBLE, src, TAG_COM_LOAD, MPI_COMM_WORLD, status);
   	// Update the load of nbr
   	nbrInfo[procId2NbrId(src)].load = load;
   	//fprintf(stdout, "[%d] Receive load status : %f from [%d]\n", myRank, load, src);
}


int countPendingJobRequest(){
	// Count pending job requests
	int iNbr, pending = 0;
	for(iNbr=0; iNbr < NB_NEIGHBORS; ++iNbr){
		if(nbrInfo[iNbr].myReqStatus == MY_JOB_REQ_ASKED){
			++pending;
		}
	}
	return pending;
}

void sendJobRequest(int nbrId, double size){
	// Immediate send (non blocking)
	MPI_Bsend(&size, 1, MPI_DOUBLE, nbrInfo[nbrId].id, TAG_REQUEST, MPI_COMM_WORLD);
	nbrInfo[nbrId].myReqStatus = MY_JOB_REQ_ASKED;
}

void receiveJobRequest(MPI_Status *status){
	int src;
	double sizeReq;

	// Get source
	src = status->MPI_SOURCE;
	// Consume message and update sizeReq
	MPI_Recv(&sizeReq, 1, MPI_DOUBLE, src, TAG_REQUEST, MPI_COMM_WORLD, status);
	// Update nbrId
	nbrInfo[procId2NbrId(src)].hisReqStatus = sizeReq;
	//fprintf(stdout, "[%d] Receive job request : %f from [%d]\n", myRank, sizeReq, src);
}

void sendJobs(int nbrId){
	MPI_Isend(sendBuff[nbrId].jobs, sendBuff[nbrId].size, jobType,
			  nbrInfo[nbrId].id, TAG_SEND_JOBS, MPI_COMM_WORLD,
			  &sendBuff[nbrId].req);
	nbrInfo[nbrId].hisReqStatus = HIS_JOB_REQ_SENT;
	fprintf(stdout, "[%d] Send %d job to [%d]\n", myRank, sendBuff[nbrId].size, nbrInfo[nbrId].id);
}

void cleanSentJobs(int immediate){
	int iNbr, flag;
	MPI_Status status;

	for(iNbr=0; iNbr<NB_NEIGHBORS; ++iNbr){
		if(nbrInfo[iNbr].hisReqStatus == HIS_JOB_REQ_SENT){
			if(immediate == 0){
				MPI_Wait(&sendBuff[iNbr].req, &status);
				sendBuff[iNbr].size = -1;
				free(sendBuff[iNbr].jobs);
				sendBuff[iNbr].jobs = NULL;
				nbrInfo[iNbr].hisReqStatus = HIS_JOB_REQ_NONE;
			} else {
				MPI_Test(&sendBuff[iNbr].req, &flag, &status);
				if(flag){
					sendBuff[iNbr].size = -1;
					free(sendBuff[iNbr].jobs);
					sendBuff[iNbr].jobs = NULL;
					nbrInfo[iNbr].hisReqStatus = HIS_JOB_REQ_NONE;
				}
			}
		}
	}
}

void receiveJobs(MPI_Status *status, int *isVectorUpdated){
	int src, nRecv;
	ptrJob_t jobs;

	// Get size, source and create array
	src = status->MPI_SOURCE;
	MPI_Get_count(status, jobType, &nRecv);
	// Create temp array
	jobs = (job_t*) malloc(nRecv * sizeof(job_t));
	// Consume message and update sizeReq
	MPI_Recv(jobs, nRecv, jobType, src, TAG_SEND_JOBS, MPI_COMM_WORLD, status);
	// Update nbrId
	nbrInfo[procId2NbrId(src)].myReqStatus = MY_JOB_REQ_NONE;
	// Insert jobs
	insertJobs(nRecv,jobs);
	free(jobs);
	if(nRecv > 0){
		 *isVectorUpdated = 1;
	}
	fprintf(stdout, "[%d] Recv %d job from [%d]\n", myRank, nRecv, src);

}

void sendMyStatus(){
	int iNbr;
	for(iNbr = 0; iNbr < NB_NEIGHBORS; ++iNbr){
		// Buffered  send (non blocking)
		MPI_Bsend(&myStatus, 1, MPI_INT, nbrInfo[iNbr].id, TAG_STATUS, MPI_COMM_WORLD);
		fprintf(stdout, "[%d] Send status %d to [%d] (his load : %f)\n", myRank, myStatus, nbrInfo[iNbr].id, nbrInfo[iNbr].load);
	}
}

void receiveStatus(MPI_Status *status){
	int src, tmp;
	// Get size, source and create array
	src = status->MPI_SOURCE;
	// Consume message and update nbrInfo
	MPI_Recv(&tmp, 1, MPI_INT, src, TAG_STATUS, MPI_COMM_WORLD, status);
	// Update nbrId
	nbrInfo[procId2NbrId(src)].status = tmp;
	nbrInfo[procId2NbrId(src)].myReqStatus = MY_JOB_REQ_NONE;
	fprintf(stdout, "[%d] Receive status %d from [%d] (my load : %f) \n", myRank, tmp, src, getTotalLoad());
}

void answerRequests(int *isVectorUpdated){
	int iNbr;
	double loadSize;
	for(iNbr = 0; iNbr < NB_NEIGHBORS; ++iNbr){
		loadSize = nbrInfo[iNbr].hisReqStatus;
		if(loadSize > 0){
			// We send at max our load  divided by 2
			loadSize = loadSize < (getTotalLoad()/2.) ? loadSize : (getTotalLoad()/2.);
			//printf("[%d] Planning to send %f to %d\n", myRank, loadSize, nbrInfo[iNbr].id);
			getLastJobs(&loadSize, &sendBuff[iNbr].size, &sendBuff[iNbr].jobs);
			sendJobs(iNbr);
			if(loadSize > 0){
				// Update neighbor load
				nbrInfo[iNbr].load += loadSize;
				*isVectorUpdated = 1;
			}
		}
	}
}

void balanceLoadRID(){
	int iNbr;
	double myLoad, meanLoad;
	float weights[NB_NEIGHBORS], sumWeights, diffMean;

	// Check if we have reached lowThreshod
	myLoad = getTotalLoad();
	if(myLoad > lowThreshold) return; // Return if not

	// We have to check the balancing
	meanLoad = myLoad;
	for(iNbr = 0; iNbr < NB_NEIGHBORS; ++iNbr){
		meanLoad += nbrInfo[iNbr].load;
	}
	meanLoad /= (NB_NEIGHBORS+1);

	// Are we significantly under the mean load
	if((meanLoad - myLoad) < diffThreshold) return; // Return if no

	// Compute weights
	sumWeights = 0.;
	for(iNbr = 0; iNbr < NB_NEIGHBORS; ++iNbr){
		// If the diff with the mean is greater than 0 we keep it
		// surplus of proc k is h_k = load_k - meanLoad
		diffMean = nbrInfo[iNbr].load - meanLoad;
		weights[iNbr] = diffMean > 0. ? diffMean : 0.;
		// H_p is sum of surplus
		sumWeights += weights[iNbr];
	}

	// Send jobs requests using weights
	// meanLoad - myLoad is what i miss
	diffMean = meanLoad - myLoad;
	for(iNbr = 0; iNbr < NB_NEIGHBORS; ++iNbr){
		// We ask for jobs if the weight is greater than 0 and we have not yet asked
		if((weights[iNbr] > 0) && (nbrInfo[iNbr].myReqStatus != MY_JOB_REQ_ASKED) && (nbrInfo[iNbr].status == STATUS_RID)){
			// weight is : h_k/H_p
			weights[iNbr] /= sumWeights;
			// Ask for : weight_k*myMiss only if greater than MINIMAL_REQUEST
			if(weights[iNbr]*diffMean > MINIMAL_REQUEST){
				sendJobRequest(iNbr, weights[iNbr]*diffMean);
			}
			//printf("[%d] asking for %f jobs to %d.\n", myRank, weights[iNbr]*diffMean, nbrInfo[iNbr].id);
		}
	}
}

void balanceLoadSID(int *isVectorUpdated){
	int iNbr;

	// For each neighbors
	for(iNbr = 0; iNbr < NB_NEIGHBORS; ++iNbr){
		if(getNJobsToProc() < 2) return;
		// We check if this neighbor is in SID mode with no pending message and 0 load
		if((nbrInfo[iNbr].status == STATUS_SID) && (nbrInfo[iNbr].hisReqStatus == HIS_JOB_REQ_NONE) && (nbrInfo[iNbr].load == 0.)){
			// If it's the case we push one job
			fprintf(stdout, "[%d] Pushing 1 job to [%d].\n", myRank, nbrInfo[iNbr].id);
			getOneJob(&sendBuff[iNbr].size, &sendBuff[iNbr].jobs);
			if(sendBuff[iNbr].size > 0){
				sendJobs(iNbr);
				*isVectorUpdated = 1;
			}

		}
	}
}

int areAllNbrSID(){
	int iNbr, nNbrSID = 0;
	// For each neighbors
	for(iNbr = 0; iNbr < NB_NEIGHBORS; ++iNbr){
		if(nbrInfo[iNbr].status == STATUS_SID){
			++nNbrSID;
		}
	}
	return nNbrSID == NB_NEIGHBORS;
}

void saveCheckPoint(int isVectorUpdated){
	FILE *checkPtFile;

	checkPtFile = fopen(checkPtFName, "w");
	fprintf(checkPtFile, "%li\n", checkpoint);
	fclose(checkPtFile);


	if(isVectorUpdated){
		writeToFile(jobFName);
	}
}
