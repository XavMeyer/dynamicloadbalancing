/*
 * JobVector.c
 *
 *  Created on: 28 juin 2013
 *      Author: meyerx
 */
#include "JobVector.h"


/*************************************************************************
 ******************           Local variables       *********************
 ************************************************************************/
long maxSizeV = -1;
ptrJob_t jobV = NULL, pBeg, pEnd;
double totalLoad;


/*************************************************************************
 ******************       Local functions headers    *********************
 ************************************************************************/

void parseJob(char *line, job_t *job);
void writeJob(job_t *job, FILE *oFile);
void resizeMemory(int nNewJobs);

/*************************************************************************
 ******************          Global functions         *********************
 ************************************************************************/
void initJobVector(char *fileName){
	readFromFile(fileName);
}

void readFromFile(char *fileName){
	char line[LONG_STR_LEN];
	long nJobs;
	ptrJob_t pCur;
	FILE *iFile;

	totalLoad = 0.;

	// Open file
	iFile = fopen(fileName, "r");
	if(iFile == NULL){
		printf("File -%s- doesn't exist.\n", fileName);
		return;
	}
	// First line should have the number of job in the file
	if(fgets(line, LONG_STR_LEN, iFile) != NULL){
		nJobs = atol(line);
		maxSizeV = nJobs*VECTOR_SIZE_MARGIN;
		// Allocate memory for the vector
		if(jobV != NULL){
			free(jobV);
		}
		jobV = (job_t*) malloc(maxSizeV*sizeof(job_t));
		pBeg = jobV;
		pEnd = pBeg + nJobs;
	}

	// Then each line contains a job
	for(pCur = pBeg; pCur != pEnd; ++pCur){
		if(fgets(line, LONG_STR_LEN, iFile) == NULL){
			printf("File -%s- doesn't contain %ld jobs.\n", fileName, nJobs);
			return;
		}
		parseJob(line, pCur);
		totalLoad += pCur->time;
	}

	fclose(iFile);
}

void writeToFile(char *fileName){
	ptrJob_t pCur;
	FILE *oFile;

	// Check if vector exists
	if(jobV == NULL){
		printf("Cannot write empty job vector into -%s-.\n", fileName);
		return;
	}

	// Open file
	oFile = fopen(fileName, "w");

	// Write nb of jobs
	fprintf(oFile, "%ld\n", getNJobsTotal());

	// Write jobs
	for(pCur = jobV; pCur != pEnd; ++pCur){
		writeJob(pCur, oFile);
	}

	fclose(oFile);
}


void deInitJobVector(){
	if(jobV != NULL){
		free(jobV);
		jobV = NULL;
	}
}


void getJob(ptrJob_t *aJobPtr){
	if(pBeg != pEnd){
		*aJobPtr = pBeg;
	} else {
		*aJobPtr = NULL;
	}
}

void jobDone(){
	if(pBeg != pEnd){
		totalLoad -= pBeg->time;
		++pBeg;
	}
}

void printJob(){
	printf("%s-%s-%i-%i : %f \n", pBeg->alignement, pBeg->tree, pBeg->pos1, pBeg->pos2, pBeg->time);
}

void getLastJobs(double *aLoad, int *aNJobs, ptrJob_t *aJobPtr){
	double loadCnt = 0.;
	ptrJob_t pCur;

	// If empty or only 1 job we send nothing
	if(getNJobsToProc() < 2){
		*aLoad = 0.;
		*aNJobs = 0;
		*aJobPtr = NULL;
		return;
	}

	// Find the limit (reverse loop from pEnd-1 to pBeg+1)
	*aNJobs = 0;
	pCur = pEnd;
	while(pCur > (pBeg+1) && loadCnt < *aLoad){
		--pCur;
		totalLoad -= pCur->time;
		loadCnt += pCur->time;
		++(*aNJobs);
	}

	// Create the job vector and copy them
	(*aJobPtr) = (job_t*) malloc(*aNJobs * sizeof(job_t));
	memcpy((*aJobPtr), pCur, *aNJobs * sizeof(job_t));

	// change the place of the pEnd pointer and the number of job in the vector
	pEnd = pCur;

	// update load
	*aLoad = loadCnt;
}

void getOneJob(int *aNJobs, ptrJob_t *aJobPtr){
	// If empty or only 1 job we send nothing
	if(getNJobsToProc() <= 1){
		*aNJobs = 0;
		*aJobPtr = NULL;
		return;
	}

	// Update vector info
	--pEnd;
	totalLoad -= pEnd->time;

	// Create the job vector and copy the last one
	*aNJobs = 1.0;
	(*aJobPtr) = (job_t*) malloc(sizeof(job_t));
	memcpy((*aJobPtr), pEnd, sizeof(job_t));
}



void insertJobs(int aNJobs, ptrJob_t aJobs){
	ptrJob_t pCur;

	if(aNJobs > 0){

		// Resize memory if needed
		if((getNJobsTotal() + aNJobs) > maxSizeV){
			resizeMemory(aNJobs);
		}

		// Insert jobs : copy memory
		memcpy(pEnd, aJobs, aNJobs * sizeof(job_t));

		// Update pointers
		pCur = pEnd;
		pEnd += aNJobs;

		// Update load
		for(; pCur != pEnd; ++pCur){
			totalLoad += pCur->time;
		}
	}
}

void skipToJob(int aIdJob){
	ptrJob_t pCur;
	// Check if we sty inside the vector
	assert(jobV + aIdJob <= pEnd);
	// Change beg poitner
	pBeg = jobV + aIdJob;
	// Update load
	for(pCur = jobV; pCur != pBeg; ++pCur){
		totalLoad -= pCur->time;
	}
}

double getTotalLoad(){
	return totalLoad;
}

double getMeanTaskGranularity(){
	if(pEnd == pBeg) return 0.;
	else return totalLoad / getNJobsToProc();
}

long getNJobsToProc(){
	return pEnd-pBeg;
}

long getNJobsProcessed(){
	return pBeg-jobV;
}

long getNJobsTotal(){
	return pEnd-jobV;
}

/*************************************************************************
 *******************       Local functions body    ***********************
 ************************************************************************/

void parseJob(char *line, job_t *job){
	sscanf(line, "%[^:]%*[:]%[^:]%*[:]%u%*[:]%u%*[:]%u%*[:]%u%*[:]%f", job->alignement, job->tree,
		   &job->pos1, &job->pos2, &job->nCSS, &job->nLeaf, &job->time);
}

void writeJob(job_t *job, FILE *oFile){
	fprintf(oFile, "%s:%s:%d:%d:%d:%d:%f\n", job->alignement, job->tree,
			   job->pos1, job->pos2, job->nCSS, job->nLeaf, job->time);
}

void resizeMemory(int nNewJobs){
	int begOffset, endOffset;
	long nNewTotal;

	// get the pointers offset
	begOffset = pBeg - jobV;
	endOffset = pEnd - jobV;

	// newTotalSize
	nNewTotal = nNewJobs + getNJobsTotal();

	// Resize
	maxSizeV = VECTOR_SIZE_MARGIN*(nNewTotal);
	jobV = (job_t*) realloc(jobV, maxSizeV*sizeof(job_t));

	// Reset pointers
	pBeg = jobV + begOffset;
	pEnd = jobV + endOffset;

}
