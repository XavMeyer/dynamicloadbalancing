/*
 * LoadBalancingMixte.h
 *
 *  Created on: 28 juin 2013
 *      Author: meyerx
 */

#ifndef LOADBALANCINMIXTE_H_
#define LOADBALANCINMIXTE_H_

#include "def.h"
#include "JobVector.h"

/******** Types and defines ********/
#define NEIGHBORHOOD_1D			0
#define NEIGHBORHOOD_2D			1
#define NEIGHBORHOOD_3D_16P		2
#define NEIGHBORHOOD_BGQ		3


#define TYPE_NEIGHBORHOOD		NEIGHBORHOOD_1D
#if TYPE_NEIGHBORHOOD == NEIGHBORHOOD_1D
	#define NB_NEIGHBORS 			 2
#elif TYPE_NEIGHBORHOOD == NEIGHBORHOOD_2D
	#define NB_NEIGHBORS 			 4
#elif TYPE_NEIGHBORHOOD == NEIGHBORHOOD_3D_16P
	#define NB_NEIGHBORS 			 6
	#define N_PROC_NODE				16
	#define X_DIM_SIZE				(N_PROC_NODE/2)
	#define Y_DIM_SIZE				(N_PROC_NODE/2)
	#define PLANE_SIZE				(X_DIM_SIZE*Y_DIM_SIZE)
#elif TYPE_NEIGHBORHOOD == NEIGHBORHOOD_BGQ
	#define N_NODE							128
	#define N_PROC_PER_NODE					(X_DIM_SIZE*Y_DIM_SIZE)
	#define A_DIM							0
	#define B_DIM							1
	#define C_DIM							2
	#define D_DIM							3
	#define E_DIM							4
	#define X_DIM							5
	#define Y_DIM							6
	#define A_DIM_SIZE						4
	#define B_DIM_SIZE						4
	#define C_DIM_SIZE						4
	#define D_DIM_SIZE						4
	#define E_DIM_SIZE						2
	#define X_DIM_SIZE						4
	#define Y_DIM_SIZE						4
	#if N_NODE == 128
		#define NB_NEIGHBORS				11
	#endif
#endif


// MPI_TAG used, 0 is free for junk message
#define TAG_COM_LOAD			1
#define TAG_REQUEST				2
#define TAG_SEND_JOBS			3
#define TAG_STATUS				4

// Job request status
#define HIS_JOB_REQ_NONE		0.
#define HIS_JOB_REQ_SENT		-1.

#define MY_JOB_REQ_NONE			0
#define MY_JOB_REQ_ASKED		1

#define STATUS_RID				0
#define STATUS_SID				1
#define STATUS_FINISHED			2

#define RATIO_LOW_THRESH 		0.50
#define UPDATE_FACTOR			0.9
#define MINIMAL_REQUEST			20.0

#define RATIO_DIFF_MEAN			1
#define RATIO_DIFF_1PCT			2
#define TYPE_RATIO_DIFF			RATIO_DIFF_MEAN


typedef struct {
	int id;
	double load;
	double hisReqStatus;
	int myReqStatus;
	int status;
} neighbor_t;

typedef struct {
	int size;
	ptrJob_t jobs;
	MPI_Request req;
} sendBuff_t;

/******** Global functions ********/
void initLBRID(int _myRank, int _nProc);
void deInitLBRID();

int askNextJob(ptrJob_t *job);
void signalJobDone();


#endif /* LOADBALANCINMIXTE_H_ */
