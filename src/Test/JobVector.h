/*
 * JobVector.h
 *
 *  Created on: 28 juin 2013
 *      Author: meyerx
 */

#ifndef JOBVECTOR_H_
#define JOBVECTOR_H_

#include "def.h"

#define VECTOR_SIZE_MARGIN		1.5

// Types
typedef struct {
	char alignement[STR_LEN];
	char tree[STR_LEN];
	unsigned int pos1;
	unsigned int pos2;
	unsigned int nCSS;
	unsigned int nLeaf;
	float time;
} job_t;

typedef job_t* ptrJob_t;


// Global functions
void initJobVector(char *fileName);
void deInitJobVector();
void writeToFile(char *fileName);
void readFromFile(char *fileName);

// First job on the stack
void getJob(ptrJob_t *aJobPtr);
void printJob();
void jobDone();
// "remove" the last jobs of the stack and return them into an array
void getLastJobs(double *aLoad, int *aNJobs, ptrJob_t *aJobPtr);
void getOneJob(int *aNJobs, ptrJob_t *aJobPtr);
// "add" jobs at the end of the stack
void insertJobs(int aNJobs, ptrJob_t aJobs);
// skip to job xx
void skipToJob(int aIdJob);

// Getters
double getTotalLoad();
double getMeanTaskGranularity();

long getNJobsToProc();
long getNJobsProcessed();
long getNJobsTotal();

#endif /* JOBVECTOR_H_ */
