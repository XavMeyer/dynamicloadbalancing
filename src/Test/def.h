/*
** def.h: Header file with global variables for the coev program.
**
** Linda Dib & Wim Hordijk   Last modified: 30 April 2013
*/

#ifndef _DEF_H_
#define _DEF_H_

#include <stdio.h> 
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <complex.h>
#include <time.h>
#include <pthread.h>
#include <sys/time.h>
#include "mpi.h"

#include <unistd.h>
#include <signal.h>
#include <assert.h>

/*
** Global variables.
*/
#define nrNt        4
#define nrNtComb   16
#define nrAa       20
#define nrAaComb  400
#define nrNtProf  192
#define nrAaProf 4000
#define nrAaProf 4000
#define MAX_LINE_LEN 10000
#define NUM_THREADS 2

#define ML          2

#define NT          1
#define AA          2

#define JC          1
#define HKY         2
#define GTR         3


#define STR_LEN				256
#define LONG_STR_LEN		1024

#define PREFIX_JOB			"job_"
#define PREFIX_CHKPT		"chkpt_"
#define PREFIX_TIME			"time_"
#define PREFIX_ERROR		"error_"
#define DATA_PATH			""
#define RES_DIR				"res/"
#define CHKPT_DIR			"chkpt/"
#define JOB_DIR				"jobs/"
#define INPUT_DIR			"inputData/"
#define TIME_DIR			"times/"
#define ERROR_DIR			"errors/"

struct node{
  char         data[8], label[128];
  double       dist, *probVector;
  struct node *parent, *left, *right;
};

struct opt{
	int*        profileVect;
	struct node *tree;
};

struct MPI_data{
	int col1;
	int col2;
	char  treeFile[1024], alignFile[1024];
};


struct sequence
{
  char            *label, *sequence;
  struct sequence *next;
};

extern int          IT, sample_freq, print_freq, burnin, dataType,
                    innerLoopIteration, nrComb,maxNrProfiles;
extern double       s, d, alpha, beta; 
extern double       w1, w2;          //JC
extern char         outFile[1024], *ntComb[nrNtComb], *aaComb[nrAaComb];

/*
** Function prototypes.
*/
int ml(int **profiles, int *nrProfiles, int *obsCombs, int col1,  int col2, struct node *root,char * outFile);

#endif  /* _DEF_H_ */
