/**
 * @file NeighborhoodFactory.h
 *
 * @date Nov 19, 2015
 * @author meyerx
 * @brief
 */
#ifndef NEIGHBORHOODFACTORY_H_
#define NEIGHBORHOODFACTORY_H_

#include "Neighbor.h"

#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <vector>
#include <algorithm>
#include <iostream>

namespace LoadBalancing {

class NeighborhoodFactory {
public:
	enum neighborhoodType_t {NBRHOOD_1D=0, NBRHOOD_2D=1, NBRHOOD_3D=2};

public:
	NeighborhoodFactory(int aMyRank);
	~NeighborhoodFactory();

	std::vector<Neighbor> create(neighborhoodType_t type, size_t nProc);
	std::vector<Neighbor> create(neighborhoodType_t type, const std::vector<int> &ranks);


private:

	static const size_t N_PROC_NODE, X_DIM_SIZE, Y_DIM_SIZE, PLANE_SIZE;
	int myRank;
	std::vector<int> createDefaultRanks(size_t nProc);

	std::vector<Neighbor> create1D(size_t nProc);
	std::vector<Neighbor> create1D(const std::vector<int> &ranks);
	std::vector<Neighbor> create2D(size_t nProc);
	std::vector<Neighbor> create2D(const std::vector<int> &ranks);
	std::vector<Neighbor> create3D_16P(size_t nProc);
	std::vector<Neighbor> create3D_16P(const std::vector<int> &ranks);

	void findDivisor2D(size_t nProc, size_t &dim1, size_t &dim2);
};

} /* namespace LoadBalancing */

#endif /* NEIGHBORHOODFACTORY_H_ */
