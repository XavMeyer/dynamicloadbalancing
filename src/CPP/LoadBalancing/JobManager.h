/**
 * @file JobManager.h
 *
 * @date Nov 19, 2015
 * @author meyerx
 * @brief
 */
#ifndef LOADBALANCING_H_
#define JOBMANAGER_H_

#include "Definitions.h"
#include "LBCommMgr.h"
#include "JobQueue.h"
#include "Helpers.h"

#include <sstream>
#include <string>
#include <vector>
#include <fstream>
#include <iomanip>

namespace LoadBalancing {

template <class T>
class JobManager {
public:
	JobManager(const std::string &aJobPrefix, const std::string &aCPPrefix, LBCommMgr &aLBMgr);
	~JobManager();

	T* askNextJob();
	void signalJobDone();

	double getCurrentLoad() const;

private:

	static const double _RATIO_LOW_THRESHOLD, _UPDATE_FACTOR, _MINIMAL_REQUEST;

	double lowThreshold, diffThreshold, communicatedLoad;

	std::string jobFN, cpFN;
	loadBalancingStatus_t myStatus;
	LBCommMgr &lbMgr;
	JobQueue jobs;
	std::vector<Neighbor> &nbrs;

	void initJobsFromFile();

	void balanceLoadRID();
	void balanceLoadSID();

	void answerRequests();
	bool areAllNbrSID();

};

} /* namespace LoadBalancing */

#endif /* LOADBALANCING_H_ */
