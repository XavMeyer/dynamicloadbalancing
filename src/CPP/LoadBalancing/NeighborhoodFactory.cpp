/**
 * @file NeighborhoodFactory.cpp
 *
 * @date Nov 19, 2015
 * @author meyerx
 * @brief
 */
#include "NeighborhoodFactory.h"

namespace LoadBalancing {

const size_t NeighborhoodFactory::N_PROC_NODE = 8;
const size_t NeighborhoodFactory::X_DIM_SIZE = N_PROC_NODE/2;
const size_t NeighborhoodFactory::Y_DIM_SIZE = N_PROC_NODE/2;
const size_t NeighborhoodFactory::PLANE_SIZE = X_DIM_SIZE*Y_DIM_SIZE;

NeighborhoodFactory::NeighborhoodFactory(int aMyRank) : myRank(aMyRank) {

}

NeighborhoodFactory::~NeighborhoodFactory() {
}

std::vector<Neighbor> NeighborhoodFactory::create(neighborhoodType_t type, size_t nProc) {
	if(type == NBRHOOD_1D) {
		return create1D(nProc);
	} else if(type == NBRHOOD_2D) {
		return create2D(nProc);
	} else if(type == NBRHOOD_3D) {
		return create3D_16P(nProc);
	} else {
		std::cerr << "Unknown neighborhood : return 1D." << std::endl;
		return create1D(nProc);
	}

}

std::vector<Neighbor> NeighborhoodFactory::create(neighborhoodType_t type, const std::vector<int> &ranks) {
	if(NBRHOOD_1D) {
		return create1D(ranks);
	} else if(NBRHOOD_2D) {
		return create2D(ranks);
	} else if(NBRHOOD_3D) {
		return create3D_16P(ranks);
	} else {
		std::cerr << "Unknown neighborhood : return 1D." << std::endl;
		return create1D(ranks);
	}
}

std::vector<Neighbor> NeighborhoodFactory::create1D(size_t nProc) {
	return create1D(createDefaultRanks(nProc));
}

std::vector<Neighbor> NeighborhoodFactory::create1D(const std::vector<int> &ranks) {
	assert(ranks.size() > 1);

	std::vector<int>::const_iterator it = std::find(ranks.begin(), ranks.end(), myRank);
	assert(it != ranks.end());

	size_t myId = std::distance(ranks.begin(), it);
	std::vector<Neighbor> nbr(2);
	nbr[0].id = ranks[(myId+1) % ranks.size()];
	nbr[1].id = ranks[(ranks.size()+myId-1) % ranks.size()];

	return nbr;
}

std::vector<Neighbor> NeighborhoodFactory::create2D(size_t nProc) {
	return create2D(createDefaultRanks(nProc));
}

std::vector<Neighbor> NeighborhoodFactory::create2D(const std::vector<int> &ranks) {
	assert(ranks.size() > 3);

	std::vector<int>::const_iterator it = std::find(ranks.begin(), ranks.end(), myRank);
	assert(it != ranks.end());

	size_t myId = std::distance(ranks.begin(), it);

	size_t sizeX, sizeY;
	findDivisor2D(ranks.size(), sizeX, sizeY);
	assert(sizeX > 1 && sizeY > 1);
	div_t divRes = div(myId, sizeX);
	size_t myY = divRes.quot;
	size_t myX = divRes.rem;

	std::vector<Neighbor> nbr(4);
	// Left neighbor
	nbr[0].id = ranks[((sizeX+myX-1)%sizeX)+myY*sizeX];
	// Right neighbor
	nbr[1].id = ranks[((myX+1)%sizeX)+myY*sizeX];
	// Up neighbor
	nbr[2].id = ranks[ myX+((sizeY+myY-1)%sizeY)*sizeX];
	// Down neighbor
	nbr[3].id = ranks[myX+((myY+1)%sizeY)*sizeX];
	//printf("[%d] My neighbors are : x=[%d] y=[%d] sX=[%d] sY[%d] l=[%d] r=[%d] u=[%d] d=[%d]\n", myRank, myX, myY, sizeX, sizeY, nbr[0].id, nbr[1].id, nbr[2].id, nbr[3].id);


	return nbr;
}

std::vector<Neighbor> NeighborhoodFactory::create3D_16P(size_t nProc) {
	return create3D_16P(createDefaultRanks(nProc));
}

std::vector<Neighbor> NeighborhoodFactory::create3D_16P(const std::vector<int> &ranks) {
	assert(ranks.size() > 7);

	std::vector<int>::const_iterator it = std::find(ranks.begin(), ranks.end(), myRank);
	assert(it != ranks.end());

	size_t nProc = ranks.size();
	size_t myId = std::distance(ranks.begin(), it);

	int myX, myY, myZ, dimSizeZ;
	div_t divResZ, divResXY, divResNProc;
	// We assume that we have at least 3*PLANE_SIZE processors this leads to
	assert(nProc >= (3*PLANE_SIZE));
	divResNProc = div(nProc, PLANE_SIZE);
	assert(divResNProc.rem == 0);

	// First we divide by the size of a X-Y plane
	// PLANE_SIZE is given by X_DIM_SIZE * Y_DIM_SIZE
	divResZ = div(myId, PLANE_SIZE);
	// We get the z position
	myZ = divResZ.quot;

	// Then we divide the remainder by the Y_DIM_SIZE
	divResXY = div(divResZ.rem, Y_DIM_SIZE);
	myY = divResXY.quot;
	myX = divResXY.rem;

	dimSizeZ = divResNProc.quot;
	// TODO MORE GENERAL CASE
	// Now we have to look if the last plane is full
	/*if(divResNProc.rem == N_PROC_NODE){
	} else if(divResNProc.rem == 2*N_PROC_NODE){
	} else if(divResNProc.rem == 3*N_PROC_NODE){
	}*/

	std::vector<Neighbor> nbr(6);
	// x,y,z-1
	nbr[0].id = ranks[myX+(myY*Y_DIM_SIZE)+((myZ+dimSizeZ-1) % dimSizeZ)*PLANE_SIZE];
	// x,y,z+1
	nbr[1].id = ranks[myX+(myY*Y_DIM_SIZE)+((myZ+1) % dimSizeZ)*PLANE_SIZE];
	// x,y-1,z
	nbr[2].id = ranks[myX+(((myY+Y_DIM_SIZE-1)%Y_DIM_SIZE)*Y_DIM_SIZE)+(myZ*PLANE_SIZE)];
	// x,y+1,z
	nbr[3].id = ranks[ myX+(((myY+1)%Y_DIM_SIZE)*Y_DIM_SIZE)+(myZ*PLANE_SIZE)];
	// x-1,y,z
	nbr[4].id = ranks[((myX+X_DIM_SIZE-1)%X_DIM_SIZE)+(myY*Y_DIM_SIZE)+(myZ*PLANE_SIZE)];
	// x+1,y,z
	nbr[5].id = ranks[((myX+1)%X_DIM_SIZE)+(myY*Y_DIM_SIZE)+(myZ*PLANE_SIZE)];

	/*printf("[%d] My neighbors are : x=[%d] y=[%d] z=[%d] n1=[%d] n2=[%d] n3=[%d] n4=[%d] n5=[%d] n6=[%d]\n",
			myRank, myX, myY, myZ, nbrInfo[0].id, nbrInfo[1].id, nbrInfo[2].id, nbrInfo[3].id,
			nbrInfo[4].id, nbrInfo[5].id);*/
	//sleep(5);
	return nbr;
}

std::vector<int> NeighborhoodFactory::createDefaultRanks(size_t nProc) {
	std::vector<int> ranks(nProc, 0);
	for(size_t i=0; i<nProc; ++i){
		ranks[i] = i;
	}
	return ranks;
}

void NeighborhoodFactory::findDivisor2D(size_t nProc, size_t &dim1, size_t &dim2) {
	size_t limit = sqrt(nProc);
	dim1 = dim2 = 0;
	for(size_t i=1; i<=limit; ++i) {
		div_t divRes = div(nProc, i);
		if(divRes.rem == 0) {
			dim1 = i;
			dim2 = divRes.quot;
		}
	}
}


} /* namespace LoadBalancing */
