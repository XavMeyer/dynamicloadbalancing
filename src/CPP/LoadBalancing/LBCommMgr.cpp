/**
 * @file LBCommMgr.cpp
 *
 * @date Nov 19, 2015
 * @author meyerx
 * @brief
 */
#include "LBCommMgr.h"

namespace LoadBalancing {

LBCommMgr::	LBCommMgr(int *argc, char ***argv, NeighborhoodFactory::neighborhoodType_t type) {

	MPI_Init(argc, argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
	MPI_Comm_size(MPI_COMM_WORLD, &nProc);

	NeighborhoodFactory nf(myRank);
	nbrs = nf.create(type, nProc);
	sendBuffers.resize(nbrs.size());
	initMpiBuffer();
}

LBCommMgr::~LBCommMgr() {
	deInitMpiBuffer();
	MPI_Finalize();
}

int LBCommMgr::getMyRank() const {
	return myRank;
}

int LBCommMgr::getNProc() const {
	return nProc;
}

std::vector<Neighbor>& LBCommMgr::getNeighbors() {
	return nbrs;
}

void LBCommMgr::setNeighbors(std::vector<Neighbor>& aNbrs) {
	deInitMpiBuffer();
	nbrs = aNbrs;
	sendBuffers.resize(nbrs.size());
	initMpiBuffer();
}

void LBCommMgr::initMpiBuffer() {
	int sizeDouble;
	// Reserve the buffer size for the outgoing messages
	MPI_Pack_size( 1, MPI_DOUBLE, MPI_COMM_WORLD, &sizeDouble);
	// 4 * nNeigbors * message type (load status + job request + finished) * overhead * buffSize
	size_t mpiBufferSize = 4 * nbrs.size() * 3 * MPI_BSEND_OVERHEAD * sizeDouble;
	mpiBuffer.resize(mpiBufferSize);
	MPI_Buffer_attach( mpiBuffer.data(), mpiBuffer.size() );
}

void LBCommMgr::deInitMpiBuffer() {
	if(!mpiBuffer.empty()) {
		int size = mpiBuffer.size();
		char *ptr = mpiBuffer.data();
		MPI_Buffer_detach(ptr, &size);
	}
}


void LBCommMgr::sendStatus(int aStatus) {
	for(size_t iNbr = 0; iNbr < nbrs.size(); ++iNbr){
		// Buffered  send (non blocking)
		MPI_Bsend(&aStatus, 1, MPI_INT, nbrs[iNbr].id, _TAG_STATUS, MPI_COMM_WORLD);
		//fprintf(stdout, "[%d] Send status %d to [%d] (his load : %f)\n", myRank, aStatus, nbrs[iNbr].id, nbrs[iNbr].load);
	}
}

void LBCommMgr::receiveStatus(MPI_Status *status) {
	int src, tmp;
	// Get size, source and create array
	src = status->MPI_SOURCE;
	// Consume message and update nbrInfo
	MPI_Recv(&tmp, 1, MPI_INT, src, _TAG_STATUS, MPI_COMM_WORLD, status);
	// Update nbrId
	nbrs[procId2NbrId(src)].status = static_cast<loadBalancingStatus_t>(tmp);
	nbrs[procId2NbrId(src)].myReqStatus = MY_JOB_REQUEST_NONE;
	//fprintf(stdout, "[%d] Receive status %d from [%d] \n", myRank, tmp, src);
}

void LBCommMgr::sendLoadStatus(double comLoad) {
	for(size_t iNbr = 0; iNbr < nbrs.size(); ++iNbr){
		// Buffered  send (non blocking)
		if(nbrs[iNbr].status == LB_STATUS_RID){
			MPI_Bsend(&comLoad, 1, MPI_DOUBLE, nbrs[iNbr].id, _TAG_COM_LOAD, MPI_COMM_WORLD);
		}
	}
}

void LBCommMgr::receiveLoadStatus(MPI_Status *status){
	int src;
	double load;
	// Check if there is pending messages
   	src = status->MPI_SOURCE;
   	// Consume message
   	MPI_Recv(&load, 1, MPI_DOUBLE, src, _TAG_COM_LOAD, MPI_COMM_WORLD, status);
   	// Update the load of nbr
   	nbrs[procId2NbrId(src)].load = load;
   	//fprintf(stdout, "[%d] Receive load status : %f from [%d]\n", myRank, load, src);
}


size_t LBCommMgr::countPendingJobRequest() {
	// Count pending job requests
	int pending = 0;
	for(size_t iNbr=0; iNbr < nbrs.size(); ++iNbr){
		if(nbrs[iNbr].myReqStatus == MY_JOB_REQUEST_ASKED){
			++pending;
		}
	}
	return pending;
}

void LBCommMgr::sendJobRequest(int nbrId, double size) {
	// Immediate send (non blocking)
	MPI_Bsend(&size, 1, MPI_DOUBLE, nbrs[nbrId].id, _TAG_REQUEST, MPI_COMM_WORLD);
	nbrs[nbrId].myReqStatus = MY_JOB_REQUEST_ASKED;
}

void LBCommMgr::receiveJobRequest(MPI_Status *status){
	int src;
	double sizeReq;

	// Get source
	src = status->MPI_SOURCE;
	// Consume message and update sizeReq
	MPI_Recv(&sizeReq, 1, MPI_DOUBLE, src, _TAG_REQUEST, MPI_COMM_WORLD, status);
	// Update nbrId
	nbrs[procId2NbrId(src)].hisReqStatus = sizeReq;
	//fprintf(stdout, "[%d] Receive job request : %f from [%d]\n", myRank, sizeReq, src);
}

void LBCommMgr::sendJob(size_t nbrId, Job::sharedPtr_t toSend){
	// Copy buffer
	SendBuffer &sb = sendBuffers[nbrId];
	assert(sb.nJobs == 0);
	sb.insertJob(toSend);

	MPI_Isend(sb.buffer.data(), sb.buffer.size(), MPI_CHAR,
			  nbrs[nbrId].id, _TAG_SEND_JOBS, MPI_COMM_WORLD,
			  &sb.req);
	nbrs[nbrId].hisReqStatus = HIS_JOB_REQUEST_SENT;
	//fprintf(stdout, "[%d] Send %d job to [%d]\n", myRank, sb.nJobs, nbrs[nbrId].id);
}


void LBCommMgr::sendJobs(size_t nbrId, std::vector<Job::sharedPtr_t> toSend){
	// Copy buffer
	SendBuffer &sb = sendBuffers[nbrId];
	assert(sb.nJobs == 0);
	for(size_t iJ=0; iJ<toSend.size(); ++iJ) {
		sb.insertJob(toSend[iJ]);
	}

	MPI_Isend(sb.buffer.data(), sb.buffer.size(), MPI_CHAR,
			  nbrs[nbrId].id, _TAG_SEND_JOBS, MPI_COMM_WORLD,
			  &sb.req);
	nbrs[nbrId].hisReqStatus = HIS_JOB_REQUEST_SENT;
	//fprintf(stdout, "[%d] Send %d job to [%d]\n", myRank, sb.nJobs, nbrs[nbrId].id);
}

int LBCommMgr::procId2NbrId(int src){
	// Not optimized but work
	for(size_t iNbr=0; iNbr<nbrs.size(); ++iNbr){
		if(nbrs[iNbr].id == (size_t)src){
			return iNbr;
		}
	}
	return -1;
}

void LBCommMgr::cleanSentJobs(bool isImmediate){
	int flag;
	MPI_Status status;

	for(size_t iNbr=0; iNbr<nbrs.size(); ++iNbr){
		if(nbrs[iNbr].hisReqStatus == HIS_JOB_REQUEST_SENT){
			if(!isImmediate){
				MPI_Wait(&sendBuffers[iNbr].req, &status);
				sendBuffers[iNbr].nJobs = 0;
				sendBuffers[iNbr].buffer.clear();
				nbrs[iNbr].hisReqStatus = HIS_JOB_REQUEST_NONE;
			} else {
				MPI_Test(&sendBuffers[iNbr].req, &flag, &status);
				if(flag){
					sendBuffers[iNbr].nJobs = 0;
					sendBuffers[iNbr].buffer.clear();
					nbrs[iNbr].hisReqStatus = HIS_JOB_REQUEST_NONE;
				}
			}
		}
	}
}

} /* namespace LoadBalancing */
