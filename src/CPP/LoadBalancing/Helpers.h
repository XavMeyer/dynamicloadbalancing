/**
 * @file Helper.h
 *
 * @date Nov 18, 2015
 * @author meyerx
 * @brief
 */
#ifndef HELPER_H_
#define HELPER_H_

#include "Jobs/Job.h"

#include <string>
#include <fstream>

namespace LoadBalancing {

void appendJobToFile(Job::sharedPtr_t aJob, const std::string &aFilename) {
	std::ofstream oFile(aFilename.c_str(), std::fstream::app);
	oFile << aJob->toString() << std::endl;
}


} /* namespace LoadBalancing */

#endif /* HELPER_H_ */
