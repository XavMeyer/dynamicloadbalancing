/**
 * @file Definitions.h
 *
 * @date Nov 19, 2015
 * @author meyerx
 * @brief
 */
#ifndef DEFINITIONS_LOADBALANCING_H_
#define DEFINITIONS_LOADBALANCING_H_

namespace LoadBalancing {

static const double HIS_JOB_REQUEST_NONE = 0.;
static const double HIS_JOB_REQUEST_SENT = -1.;

enum myJobRequest_t { MY_JOB_REQUEST_NONE=0, MY_JOB_REQUEST_ASKED=1};
enum loadBalancingStatus_t {LB_STATUS_RID=0, LB_STATUS_SID=1, LB_STATUS_DONE=2};



} /* end namespace LoadBalancing */

#endif /* DEFINITIONS_LOADBALANCING_H_ */
