/**
 * @file LBCommMgr.h
 *
 * @date Nov 19, 2015
 * @author meyerx
 * @brief
 */
#ifndef LBCOMMMGR_H_
#define LBCOMMMGR_H_

#include "Definitions.h"
#include "SendBuffer.h"
#include "Neighbor.h"
#include "NeighborhoodFactory.h"

#include <mpi.h>
#include <vector>

namespace LoadBalancing {

class LBCommMgr {
public:
	LBCommMgr(int *argc, char ***argv, NeighborhoodFactory::neighborhoodType_t type=NeighborhoodFactory::NBRHOOD_1D);
	~LBCommMgr();

	int getMyRank() const;
	int getNProc() const;
	std::vector<Neighbor>& getNeighbors();
	std::vector<SendBuffer>& getSendBuffers();

	void setNeighbors(std::vector<Neighbor>& aNbrs);

	void sendStatus(int aStatus);
	void sendLoadStatus(double comLoad);

	size_t countPendingJobRequest();
	void sendJobRequest(int nbrId, double size);

	void sendJob(size_t nbrId, Job::sharedPtr_t toSend);
	void sendJobs(size_t nbrId, std::vector<Job::sharedPtr_t> toSend);

	void cleanSentJobs(bool isImmediate);

	// Template public fonction are at the end of the header

	// template <class T>
	// void waitMessages(std::vector<Job::sharedPtr_t> &recJobs);

	// template <class T>
	// void checkPendingMessages(std::vector<Job::sharedPtr_t> &recJobs);

	// template <class T>
	// void receiveJobs(MPI_Status *status, std::vector<Job::sharedPtr_t> &recJobs);


private:

	enum tag_t {_TAG_COM_LOAD=101, _TAG_REQUEST=102, _TAG_SEND_JOBS=103, _TAG_STATUS=104};

	int myRank, nProc;
	std::vector<char> mpiBuffer;
	std::vector<Neighbor> nbrs;
	std::vector<SendBuffer> sendBuffers;

	void initMpiBuffer();
	void deInitMpiBuffer();

	void receiveStatus(MPI_Status *status);
	void receiveLoadStatus(MPI_Status *status);
	void receiveJobRequest(MPI_Status *status);

	int procId2NbrId(int src);

public:
	template <class T>
	void waitMessages(std::vector<Job::sharedPtr_t> &recJobs) {
		MPI_Status status;

			MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			switch (status.MPI_TAG) {
				case _TAG_COM_LOAD:
					//fprintf(stdout, "[%d] Processing a LOAD msg from blocking wait.\n", myRank);
					receiveLoadStatus(&status);
					break;
				case _TAG_REQUEST:
					//fprintf(stdout, "[%d] Processing a REQ msg from blocking wait.\n", myRank);
					receiveJobRequest(&status);
					break;
				case _TAG_SEND_JOBS:
					//fprintf(stdout, "[%d] Processing a JOB msg from blocking wait.\n", myRank);
					receiveJobs<T>(&status, recJobs);
					break;
				case _TAG_STATUS:
					//fprintf(stdout, "[%d] Processing a STATUS msg from blocking wait.\n", myRank);
					receiveStatus(&status);
					break;
				default:
					//fprintf(stdout, "[%d] Processing a ERR msg from blocking wait.\n", myRank);
					std::cerr << "Erroneous MPI message received." << std::endl;
					std::cerr << "Other MPI communications could cause that. Should be fixed" << std::endl;
					MPI_Abort(MPI_COMM_WORLD, 1);
					break;
			}
	}

	template <class T>
	void checkPendingMessages(std::vector<Job::sharedPtr_t> &recJobs) {
		int flag;
		MPI_Status status;

		MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &status);
		while(flag){
			switch (status.MPI_TAG) {
				case _TAG_COM_LOAD:
					receiveLoadStatus(&status);
					break;
				case _TAG_REQUEST:
					receiveJobRequest(&status);
					break;
				case _TAG_SEND_JOBS:
					receiveJobs<T>(&status, recJobs);
					break;
				case _TAG_STATUS:
					receiveStatus(&status);
					break;
				default:
					std::cerr << "Erroneous MPI message received." << std::endl;
					std::cerr << "Other MPI communications could cause that. Should be fixed" << std::endl;
					MPI_Abort(MPI_COMM_WORLD, 1);
					break;
			}
			MPI_Iprobe(MPI_ANY_SOURCE, _TAG_COM_LOAD, MPI_COMM_WORLD, &flag, &status);
		}
	}

	template <class T>
	void receiveJobs(MPI_Status *status, std::vector<Job::sharedPtr_t> &recJobs){
		Job::sharedPtr_t refJob(new T());

		// Get size, source and create array
		size_t serSize = refJob->getSerializedSize();
		size_t nRecv = status->count/serSize;
		int src = status->MPI_SOURCE;

		// Receive message
		Utils::Serialize::buffer_t buffer(status->count);
		MPI_Recv(buffer.data(), buffer.size(), MPI_CHAR, src, _TAG_SEND_JOBS, MPI_COMM_WORLD, status);

		for(size_t iR=0; iR<nRecv; ++iR) {
			Utils::Serialize::buffer_t serializedJob(buffer.begin()+iR*serSize, buffer.begin()+(iR+1)*serSize);
			Job::sharedPtr_t aJob(new T());
			aJob->unserialize(serializedJob);
			recJobs.push_back(aJob);
		}

		nbrs[procId2NbrId(src)].myReqStatus = MY_JOB_REQUEST_NONE;
		//printf("[%d] Recv %zu job from [%d]\n", myRank, nRecv, src);
	}

};

} /* namespace LoadBalancing */

#endif /* LBCOMMMGR_H_ */
