/**
 * @file SendBuffer.h
 *
 * @date Nov 19, 2015
 * @author meyerx
 * @brief
 */
#ifndef SENDBUFFER_H_
#define SENDBUFFER_H_

#include "Jobs/Job.h"

#include <mpi.h>
#include <vector>

namespace LoadBalancing {

class SendBuffer {
public:

public:
	SendBuffer() : nJobs(0), req(MPI_REQUEST_NULL) {}
	~SendBuffer() {}

	void insertJob(Job::sharedPtr_t aJob) {
		using Utils::Serialize::buffer_t;
		const buffer_t &aB = aJob->serialize();
		buffer.insert(buffer.end(), aB.begin(), aB.end());
		nJobs++;
	}

	void insertJobs(std::vector<Job::sharedPtr_t> aJobs) {
		for(size_t iJ=0; iJ<aJobs.size(); ++iJ) {
			insertJob(aJobs[iJ]);
		}
	}

public:
	size_t nJobs;
	Utils::Serialize::buffer_t buffer;
	MPI_Request req;

};

} /* namespace LoadBalancing */

#endif /* SENDBUFFER_H_ */
