/**
 * @file JobManager.cpp
 *
 * @date Nov 19, 2015
 * @author meyerx
 * @brief
 */
#include "JobManager.h"

namespace LoadBalancing {

template <class T>
const double JobManager<T>::_RATIO_LOW_THRESHOLD = 0.8;
template <class T>
const double JobManager<T>::_UPDATE_FACTOR = 0.9;
template <class T>
const double JobManager<T>::_MINIMAL_REQUEST = 5.;

template <class T>
JobManager<T>::JobManager(const std::string &aJobPrefix, const std::string &aCPPrefix, LBCommMgr &aLBMgr) :
						myStatus(LB_STATUS_RID), lbMgr(aLBMgr), nbrs(lbMgr.getNeighbors()) {

	// Create filenames
	std::stringstream ssJob, ssCP;
	ssJob << aJobPrefix << std::setw(5) << std::setfill('0') << lbMgr.getMyRank();
	ssCP << aCPPrefix << std::setw(5) << std::setfill('0') << lbMgr.getMyRank();

	jobFN = ssJob.str();
	cpFN = ssCP.str();

	// Read from file
	initJobsFromFile();

}

template <class T>
JobManager<T>::~JobManager() {
	bool isImmediate = false;
	lbMgr.cleanSentJobs(isImmediate);
}

template <class T>
void JobManager<T>::initJobsFromFile() {

	// Read from file
	jobs.readFromFile<T>(jobFN);

	// Prepare threshold etc.
	lowThreshold = _RATIO_LOW_THRESHOLD*jobs.getTotalLoad();
	diffThreshold = 1.5*jobs.getMeanTaskGranularity();

	communicatedLoad = jobs.getTotalLoad();
	lbMgr.sendLoadStatus(communicatedLoad);
}

template <class T>
T* JobManager<T>::askNextJob() {
	Job::sharedPtr_t ptrJob;

	// Get current job
	if(!jobs.empty()) {
		return static_cast<T*>(jobs.front().get());
	} else {
		// ... and my status is RID
		if(myStatus == LB_STATUS_RID ){
			// I wait for all pending request
			while(lbMgr.countPendingJobRequest() > 0){
				// Blocking
				answerRequests();

				std::vector<Job::sharedPtr_t> recJobs;
				lbMgr.waitMessages<T>(recJobs);
				jobs.push(recJobs);

				// Has the message updated the vector ?
				if(jobs.hasChanged()){
					// we save the new state of job vector
					jobs.writeToFile(jobFN);
					if(!jobs.empty()) {
						return static_cast<T*>(jobs.front().get());
					}
				} // else we wait for more message
			}
			// no more pending job request and still no jobs, we switch to SID
			myStatus = LB_STATUS_SID;
			lbMgr.sendStatus(static_cast<int>(myStatus));
		}
		if(myStatus == LB_STATUS_SID){
			// I wait for all my neighbors to be in SID mode
			while(!areAllNbrSID()){
				// Blocking
				answerRequests();

				std::vector<Job::sharedPtr_t> recJobs;
				lbMgr.waitMessages<T>(recJobs); // Wait for a message
				jobs.push(recJobs);

				// Has the message updated the vector ?
				if(jobs.hasChanged()){
					// we save the new state of job vector
					jobs.writeToFile(jobFN);
					if(!jobs.empty()) {
						return static_cast<T*>(jobs.front().get());
					}
				} // else we wait for more message
			}
			// All my neighbors are waiting for push, I'm done
			myStatus = LB_STATUS_DONE;
		}
	}
	return NULL;
}

template <class T>
void JobManager<T>::signalJobDone() {
	// Signal job done so we skip to next
	Job::sharedPtr_t doneJob(jobs.popFront());
	appendJobToFile(doneJob, cpFN);

	// Check messages
	std::vector<Job::sharedPtr_t> recJobs;
	lbMgr.checkPendingMessages<T>(recJobs);
	jobs.push(recJobs);
	if(jobs.hasChanged()){
		// we save the new state of job vector
		jobs.writeToFile(jobFN);
	} // else we wait for more message

	// Answer requests (RID mode)
	answerRequests();

	// Clean buffer for sent jobs (1 ==> not blocking)
	bool isImmediate = true;
	lbMgr.cleanSentJobs(isImmediate);

	// Then we look if we have to ask for jobs...
	if(myStatus == LB_STATUS_RID){
		balanceLoadRID();
	}

	// ..or to push some (depend on neighbor status)
	balanceLoadSID();

	// Send status if load variation is greater than 10%
	if((jobs.getTotalLoad() > communicatedLoad*(2.-_UPDATE_FACTOR)) ||
	   (jobs.getTotalLoad() < communicatedLoad*_UPDATE_FACTOR)){
		communicatedLoad = jobs.getTotalLoad();
		lbMgr.sendLoadStatus(communicatedLoad);
	}
}

template <class T>
double JobManager<T>::getCurrentLoad() const {
	return jobs.getTotalLoad();
}

template <class T>
void JobManager<T>::balanceLoadRID(){
	double myLoad, meanLoad;
	float weights[nbrs.size()], sumWeights, diffMean;

	// Check if we have reached lowThreshod
	myLoad = jobs.getTotalLoad();
	if(myLoad > lowThreshold) return; // Return if not

	// We have to check the balancing
	meanLoad = myLoad;
	for(size_t iNbr = 0; iNbr < nbrs.size(); ++iNbr){
		meanLoad += nbrs[iNbr].load;
	}
	meanLoad /= (nbrs.size()+1);

	// Are we significantly under the mean load
	if((meanLoad - myLoad) < diffThreshold) return; // Return if no

	// Compute weights
	sumWeights = 0.;
	for(size_t iNbr = 0; iNbr < nbrs.size(); ++iNbr){
		// If the diff with the mean is greater than 0 we keep it
		// surplus of proc k is h_k = load_k - meanLoad
		diffMean = nbrs[iNbr].load - meanLoad;
		weights[iNbr] = diffMean > 0. ? diffMean : 0.;
		// H_p is sum of surplus
		sumWeights += weights[iNbr];
	}

	// Send jobs requests using weights
	// meanLoad - myLoad is what i miss
	diffMean = meanLoad - myLoad;
	for(size_t iNbr = 0; iNbr < nbrs.size(); ++iNbr){
		// We ask for jobs if the weight is greater than 0 and we have not yet asked
		if((weights[iNbr] > 0) && (nbrs[iNbr].myReqStatus != MY_JOB_REQUEST_ASKED) && (nbrs[iNbr].status == LB_STATUS_RID)){
			// weight is : h_k/H_p
			weights[iNbr] /= sumWeights;
			// Ask for : weight_k*myMiss only if greater than MINIMAL_REQUEST
			if(weights[iNbr]*diffMean > _MINIMAL_REQUEST){
				lbMgr.sendJobRequest(iNbr, weights[iNbr]*diffMean);
			}
			//printf("[%d] asking for %f jobs to %zu.\n", lbMgr.getMyRank(), weights[iNbr]*diffMean, nbrs[iNbr].id);
		}
	}
}

template <class T>
void JobManager<T>::balanceLoadSID() {

	// For each neighbors
	for(size_t iNbr = 0; iNbr < nbrs.size(); ++iNbr){
		if(jobs.size() < 2) return;
		// We check if this neighbor is in SID mode with no pending message and 0 load
		if((nbrs[iNbr].status == LB_STATUS_SID) && (nbrs[iNbr].hisReqStatus == HIS_JOB_REQUEST_NONE) && (nbrs[iNbr].load == 0.)){
			// If it's the case we push one job
			if(jobs.size() > 1) {
				//printf("[%d] Pushing 1 job to [%zu].\n", lbMgr.getMyRank(), nbrs[iNbr].id);
				lbMgr.sendJob(iNbr, jobs.popBack());
			}
		}
	}
}

template <class T>
void JobManager<T>::answerRequests() {
	double loadSize;
	for(size_t iNbr = 0; iNbr < nbrs.size(); ++iNbr){
		loadSize = nbrs[iNbr].hisReqStatus;
		if(loadSize > 0){
			// We send at max our load  divided by 2
			loadSize = loadSize < (jobs.getTotalLoad()/2.) ? loadSize : (jobs.getTotalLoad()/2.);
			//printf("[%d] Sending %f to %zu\n", lbMgr.getMyRank(), loadSize, nbrs[iNbr].id);
			lbMgr.sendJobs(iNbr,jobs.popLast(loadSize));
			if(loadSize > 0){
				// Update neighbor load
				nbrs[iNbr].load += loadSize;
			}
		}
	}
}

template <class T>
bool JobManager<T>::areAllNbrSID(){
	size_t nNbrSID = 0;
	// For each neighbors
	for(size_t iNbr = 0; iNbr < nbrs.size(); ++iNbr){
		if(nbrs[iNbr].status == LB_STATUS_SID){
			++nNbrSID;
		}
	}
	return nNbrSID == nbrs.size();
}

} /* namespace LoadBalancing */
