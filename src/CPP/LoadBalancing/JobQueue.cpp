/**
 * @file JobQueue.cpp
 *
 * @date Nov 18, 2015
 * @author meyerx
 * @brief
 */
#include "JobQueue.h"

namespace LoadBalancing {

JobQueue::JobQueue() {
	totalLoad = 0;
	requireWrite = false;
}


JobQueue::~JobQueue() {
}


void JobQueue::push(Job::sharedPtr_t aJobPtr){
	totalLoad += aJobPtr->getLoad();
	jobs.push_back(aJobPtr);
	requireWrite = true;
}

void JobQueue::push(std::vector<Job::sharedPtr_t> &aJobs) {
	for(size_t iJ=0; iJ<aJobs.size(); iJ++) {
		push(aJobs[iJ]);
	}
}


Job::sharedPtr_t JobQueue::front() {
	assert(!empty());
	return jobs.front();
}

Job::sharedPtr_t JobQueue::popFront() {
	assert(!empty());
	Job::sharedPtr_t job(jobs.front());
	jobs.pop_front();
	totalLoad -= job->getLoad();
	requireWrite = true;
	return job;
}

Job::sharedPtr_t JobQueue::back() {
	assert(!empty());
	return jobs.back();
}

Job::sharedPtr_t JobQueue::popBack() {
	assert(!empty());
	Job::sharedPtr_t job(jobs.back());
	jobs.pop_back();
	totalLoad -= job->getLoad();
	requireWrite = true;
	return job;
}

std::vector<Job::sharedPtr_t> JobQueue::popLast(double aLoad) {
	std::vector<Job::sharedPtr_t> nLastJobs;

	if(jobs.size() > 1) {
		double loadCnt = 0.;
		while((nLastJobs.size() < (jobs.size()-1)) && loadCnt < aLoad) {
			nLastJobs.push_back(popBack());
			loadCnt += nLastJobs.back()->getLoad();
		}
	}
	requireWrite = true;
	return nLastJobs;
}

bool JobQueue::empty() const {
	return jobs.empty();
}

size_t JobQueue::size() const {
	return jobs.size();
}

double JobQueue::getTotalLoad() const {
	return totalLoad;
}

double JobQueue::getMeanTaskGranularity() const {
	if(jobs.empty()) return 0.;
	else return totalLoad / jobs.size();
}

bool JobQueue::hasChanged() const {
	return requireWrite;
}

void JobQueue::writeToFile(const std::string &aFilename) {
	std::ofstream oFile(aFilename.c_str());
	for(jobCtn_t::iterator it = jobs.begin(); it != jobs.end(); ++it) {
		oFile << (*it)->toString() << std::endl;
	}
	requireWrite = false;
}

} /* namespace LoadBalancing */
