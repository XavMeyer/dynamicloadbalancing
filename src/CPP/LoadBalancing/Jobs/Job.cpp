/**
 * @file Job.cpp
 *
 * @date Nov 18, 2015
 * @author meyerx
 * @brief
 */
#include "Job.h"

namespace LoadBalancing {

Job::Job() {
	myLoad = 0;
	serializedSize = 0;
}

Job::~Job() {
}

double Job::getLoad() const {
	return myLoad;
}

void Job::setLoad(double aLoad) {
	myLoad = aLoad;
}

size_t Job::getSerializedSize() const {
	return serializedSize;
}

} /* namespace LoadBalancing */
