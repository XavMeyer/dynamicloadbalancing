/**
 * @file ExampleJob.h
 *
 * @date Nov 18, 2015
 * @author meyerx
 * @brief
 */
#ifndef EXAMPLEJOB_H_
#define EXAMPLEJOB_H_

#include "Job.h"

#include <sstream>
#include <boost/algorithm/string.hpp>

namespace LoadBalancing {

class ExampleJob: public Job {
	typedef boost::shared_ptr<Job> sharedPtr_t;

public:
	ExampleJob();
	~ExampleJob();

	size_t getId() const;
	std::string getName() const;
	double getTime() const;

	void setName(const std::string &aName);
	void setTime(double aTime);

	// Help for input/output to file
	std::string toString() const;
	void fromString(const std::string &aString);

	// Helper for MPI
	const Utils::Serialize::buffer_t& serialize();
	void unserialize(const Utils::Serialize::buffer_t &aBuffer);

private:

	static size_t idSeq;
	size_t id;
	char name[256];
	double time;

	// Serialization
    friend class boost::serialization::access;

    template<class Archive>
    void save(Archive & ar, const unsigned int version) const;

    template<class Archive>
    void load(Archive & ar, const unsigned int version);
    BOOST_SERIALIZATION_SPLIT_MEMBER()

};

} /* namespace LoadBalancing */

#endif /* EXAMPLEJOB_H_ */
