/**
 * @file Job.h
 *
 * @date Nov 18, 2015
 * @author meyerx
 * @brief
 */
#ifndef JOB_H_
#define JOB_H_

#include "CPP/Utils/Code/SerializationSupport.h"

#include <string>
#include <boost/shared_ptr.hpp>

namespace LoadBalancing {

class Job {
public:
typedef boost::shared_ptr<Job> sharedPtr_t;

public:
	Job();
	virtual ~Job();

	// Help for input/output to file
	virtual std::string toString() const = 0;
	virtual void fromString(const std::string &aString) = 0;

	// Helper for MPI
	size_t getSerializedSize() const;
	virtual const Utils::Serialize::buffer_t& serialize() = 0;
	virtual void unserialize(const Utils::Serialize::buffer_t &aBuffer) = 0;

	virtual double getLoad() const;
	void setLoad(double aLoad);


protected:
	double myLoad;
	size_t serializedSize;
	Utils::Serialize::buffer_t serializedBuffer;
};

} /* namespace LoadBalancing */

#endif /* JOB_H_ */
