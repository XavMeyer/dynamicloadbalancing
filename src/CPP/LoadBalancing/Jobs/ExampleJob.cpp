/**
 * @file ExampleJob.cpp
 *
 * @date Nov 18, 2015
 * @author meyerx
 * @brief
 */
#include "ExampleJob.h"

namespace LoadBalancing {

size_t ExampleJob::idSeq = 0;

ExampleJob::ExampleJob() : Job() {
	id = idSeq++;
	time = 0.;

	std::stringstream ss;
	ss << "Job_" << id;
	std::strcpy(name, ss.str().c_str());

	serialize();
	serializedSize = serializedBuffer.size();
}

ExampleJob::~ExampleJob() {
}

size_t ExampleJob::getId() const {
	return id;
}

std::string ExampleJob::getName() const {
	return std::string(name);
}

double ExampleJob::getTime() const {
	return time;
}

void ExampleJob::setName(const std::string &aName) {
	std::strcpy(name, aName.c_str());
}

void ExampleJob::setTime(double aTime) {
	time = aTime;
}

// Help for input/output to file
std::string ExampleJob::toString() const {
	std::stringstream ss;
	ss << id << "\t" << myLoad << "\t" << name << "\t" << time;
	return ss.str();
}

void ExampleJob::fromString(const std::string &aString) {
	std::vector<std::string> words;
	boost::split(words, aString, boost::is_any_of("\t "));
	//std::cout << "Words : " << words.size() << std::endl;
	//for(size_t i=0; i<words.size(); ++i) std::cout << words[i] << std::endl;
	assert(words.size() == 4);
	id = atol(words[0].c_str());
	myLoad = atof(words[1].c_str());
	std::strcpy(name, words[2].c_str());
	time = atof(words[3].c_str());
}

// Helper for MPI
const Utils::Serialize::buffer_t& ExampleJob::serialize() {
	serializedBuffer.clear();
	Utils::Serialize::save(*this, serializedBuffer);
	assert(serializedSize == 0 || serializedBuffer.size() == serializedSize);
	return serializedBuffer;
}

void ExampleJob::unserialize(const Utils::Serialize::buffer_t &aBuffer) {
	Utils::Serialize::load(aBuffer, *this);
}

template<class Archive>
void ExampleJob::save(Archive & ar, const unsigned int version) const {
	ar & id;
	ar & myLoad;
	ar & name;
	ar & time;
}

template<class Archive>
void ExampleJob::load(Archive & ar, const unsigned int version) {
	ar & id;
	ar & myLoad;
	ar & name;
	ar & time;
}

} /* namespace LoadBalancing */
