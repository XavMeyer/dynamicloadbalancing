/**
 * @file Neighbor.h
 *
 * @date Nov 19, 2015
 * @author meyerx
 * @brief
 */
#ifndef NEIGHBOR_H_
#define NEIGHBOR_H_

#include "Definitions.h"

#include <stdlib.h>

namespace LoadBalancing {

class Neighbor {
public:
	Neighbor() : id(0), load(-1.), hisReqStatus(HIS_JOB_REQUEST_NONE),
				 myReqStatus(MY_JOB_REQUEST_NONE), status(LB_STATUS_RID) {}
	~Neighbor() {}
public:
	size_t id;
	double load, hisReqStatus;
	myJobRequest_t myReqStatus;
	loadBalancingStatus_t status;

};

} /* namespace LoadBalancing */

#endif /* NEIGHBOR_H_ */
