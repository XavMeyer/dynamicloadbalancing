/**
 * @file JobQueue.h
 *
 * @date Nov 18, 2015
 * @author meyerx
 * @brief
 */
#ifndef JOBQUEUE_H_
#define JOBQUEUE_H_

#include "Jobs/Job.h"

#include <vector>
#include <list>
#include <string>
#include <fstream>

namespace LoadBalancing {

class JobQueue {
public:
	JobQueue();
	~JobQueue();

	// Add jobs
	void push(Job::sharedPtr_t aJobPtr);
	void push(std::vector<Job::sharedPtr_t> &aJobs);

	// Access top of list
	Job::sharedPtr_t front();
	Job::sharedPtr_t popFront();

	// Access back of list
	Job::sharedPtr_t back();
	Job::sharedPtr_t popBack();
	std::vector<Job::sharedPtr_t> popLast(double aLoad);

	// Getters
	bool empty() const;
	size_t size() const;
	double getTotalLoad() const;
	double getMeanTaskGranularity() const;

	bool hasChanged() const;
	void cleanChangedFlag();

	void writeToFile(const std::string &aFilename);

	// template <class T>
	// void readFromFile(const std::string &aFilename);

private:

	bool requireWrite;
	double totalLoad;

	typedef std::list<Job::sharedPtr_t> jobCtn_t;
	jobCtn_t jobs;



public: // Template function
	template <class T>
	void readFromFile(const std::string &aFilename) {
		std::ifstream iFile(aFilename.c_str());

		std::string str;
		while (std::getline(iFile, str)) {
			if(!str.empty()) {
				Job::sharedPtr_t job(new T());
				job->fromString(str);
				push(job);
			}
		}
	}
};

} /* namespace LoadBalancing */

#endif /* JOBQUEUE_H_ */
