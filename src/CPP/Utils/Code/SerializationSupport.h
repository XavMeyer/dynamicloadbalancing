/**
 * @file SerializationSupport.h
 *
 * @date Nov 6, 2015
 * @author meyerx
 * @brief
 */
#ifndef SERIALIZATIONSUPPORT_H_
#define SERIALIZATIONSUPPORT_H_

#include <vector>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/stream_buffer.hpp>
#include <boost/iostreams/device/back_inserter.hpp>

namespace Utils {
namespace Serialize {

typedef std::vector<char> buffer_t;
typedef std::vector<buffer_t> vecBuffer_t;

template <class T>
void save(T &object, buffer_t &buffer) {
	namespace io = boost::iostreams;
	io::stream<io::back_insert_device<std::vector<char> > > output_stream(buffer);
	boost::archive::binary_oarchive oa(output_stream);

	oa << object;
	output_stream.flush();
}

template <class T>
void load(const buffer_t &buffer, T &object) {
	namespace io = boost::iostreams;
	io::basic_array_source<char> source(&buffer[0], buffer.size());
	io::stream<io::basic_array_source<char> > input_stream(source);
	boost::archive::binary_iarchive ia(input_stream);
	ia >> object;
}




} /* namespace Serialize */
} /* namespace Utils */

#endif /* SERIALIZATIONSUPPORT_H_ */
